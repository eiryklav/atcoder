#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

int h, m, H, M;

bool solve() {
  rep(i, 12) rep(j, 60) {
    int hh = H + i, mm = M + j;
    auto safe = [&](){ return hh < h || (hh == h && mm <= m); };
    if(hh >= 12 || mm >= 60) { continue; }
    if(safe()) { return true; }
    (hh += 6) %= 12;
    (mm += 30) %= 60;
    if(safe()) { return true; }
  }
  return false;
}

int main() {

  cin >> h >> m;
  cin >> H >> M;

  cout << (solve() ? "Yes" : "No") << endl;

  return 0;
}