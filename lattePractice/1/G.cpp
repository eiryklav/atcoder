#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

namespace tree {

template<class value_type>
struct segtree {
  int N; 
  vector<value_type> dat, sum;
  segtree(int n) {
    N = 1;
    while(N < n) N *= 2;
    dat.assign(2 * N - 1, 0);
    sum.assign(2 * N - 1, 0);
  }
 
  void add(int a, int b, value_type x) { add(a, b, x, 0, 0, N); }
  void update(int a, value_type x) { add(a, a+1, x, 0, 0, N); }
 
  value_type add(int a, int b, value_type x, int k, int l, int r) {
    if(b <= l || r <= a) { return dat[k]; }
    if(a <= l && r <= b) {
      sum[k] += x;
      return dat[k] += x;
    }
    int m = (l + r) / 2;
    return dat[k] = add(a, b, x, 2 * k + 1, l, m) + add(a, b, x, 2 * k + 2, m, r) + sum[k];
  }

  value_type get(int a) { return getSum(a, a+1, 0, 0, N); }
  value_type getSum(int a, int b) { return getSum(a, b, 0, 0, N); }
  value_type getSum(int a, int b, int k, int l, int r) {
    if(b <= l || r <= a) { return 0; }
    if(a <= l && r <= b) { return dat[k]; }
    int m = (l + r) / 2;
    return getSum(a, b, 2 * k + 1, l, m) + getSum(a, b, 2 * k + 2, m, r) + sum[k];
  }
 
};

} // namespace tree

#define DEBUG cout << endl; rep(i, top+1) { cout << st.get(i) << ", "; }cout << endl;


tree::segtree<ll> st(200010);
int N, M; ll H;
int top;

string challenge(ll arg) {
  ll HB = arg - H, HT = arg + H;

  ll topPos = st.getSum(0, top);
  if(HB > topPos) { return "miss"; }
  if(top >= 1 && st.getSum(0, top-1) <= HB && HB <= topPos) {
    st.update(top, -st.get(top));
//    top --;
    return "go";
  }

  int l = 0, h = top;
  while(l + 1 < h) {
    int m = (l + h) / 2;
    int sum = st.getSum(0, m);
    cout << "m: " << m << ", sum: " << sum << endl;
    if(sum <= HT) {
      l = m;
    } else {
      h = m;
    }
  }

  cout << "bsearch l: " << l << endl;
  cout << "[0, " << l << "]: " << st.getSum(0, l) << endl;

  if(st.getSum(0, l) >= HB + 1) {

    int l = 0, h = top;
    while(l + 1 < h) {
      int m = (l + h) / 2;
      int sum = st.getSum(0, m);
      cout << "m: " << m << ", sum: " << sum << endl;
      if(sum >= HB) {
        l = m;
      } else {
        h = m;
      }
    }

    if(st.getSum(0, l) <= HT - 1) {
      return "stop";
    } else {
      cout << "KK\n";
      st.add(l-1, top + 1, -st.get(l-1));
//      top --;
      return "go";
    }

  } else {
    st.add(l, top, -st.get(l));
//    top --;
    return "go";
  }
}

int main() {

  cin >> N >> M >> H;
  rep(i, N) {
    int x; cin >> x;
    st.update(i, x);
  }

  top = N - 1;
  
  DEBUG

  rep(i, M) {
    string op; ll arg; cin >> op >> arg;
    if(op[0] == 'a') {
      st.update(top + 1, arg);
      top ++;
    }
    else {
      cout << challenge(arg) << endl;
      DEBUG
    }
  }

  return 0;
}



