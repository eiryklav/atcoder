#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

int N, M, L;
string msg;
vector<string> F;

vector<tuple<int, int, int, int, int>> G[101][101][101];

vector<pair<int, int>> make_graph() {
  vector<pair<int, int>> ret;
  rep(i, L-1) {
    char from = msg[i], to = msg[i+1];
    rep(a, N) rep(b, M) {
      if(F[a][b] == from) {
        rep(dir, 4) {
          int y = a, x = b;
          while(1) {
            int ny = y + dy[dir], nx = x + dx[dir];
            if(!in_range(ny, nx, N, M)) { break; }
            if(F[ny][nx] == to) {
              G[a][b][i].emplace_back(ny, nx, i+1, abs(ny - a) + abs(nx - b), dir);
              if(i == 0) {
                ret.emplace_back(a, b);
              }
              break;
            }
            y = ny, x = nx;
          }
        }
      }
    }
  }
  return ret;
}

template<class T> using PQ_G = priority_queue<T, vector<T>, greater<T>>;

int solve() {

  auto sv = make_graph();

  int ans = inf;

  PQ_G<tuple<int, int, int, int, int>> q;
  vector<vector<vector<vector<int>>>> dist(N, vector<vector<vector<int>>>(M, vector<vector<int>>(L, vector<int>(4, inf))));
  for(auto && e: sv) { q.emplace(0, 0, e.first, e.second, 0); dist[e.first][e.second][0][0] = 0; }
  while(!q.empty()) {
    auto p = q.top(); q.pop();
    int y, x, l, c, dir; tie(c, l, y, x, dir) = p;

    if(l == L-1) {
      minimize(ans, c);
      break;
    }

    for(auto && e: G[y][x][l]) {
      int ny, nx, nl, ndc, ndir; tie(ny, nx, nl, ndc, ndir) = e;
      int nc = c + ndc;
      if(l != 0 && dir == ndir) { continue; }
      if(dist[ny][nx][nl][ndir] <= nc) { continue; }
      dist[ny][nx][nl][ndir] = nc;
      q.emplace(nc, nl, ny, nx, ndir);
    }
  }

  return ans == inf ? -1 : ans + 1;
}

int main() {

  cin >> N >> M >> L;
  cin >> msg;
  F.resize(N);
  rep(i, N) {
    cin >> F[i];
  }

  cout << solve() << endl;

  return 0;
}