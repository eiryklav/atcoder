#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int count_components(vector<vector<pair<int, int>>>& g) {

  int N = g.size();
  vector<bool> vis(N);
  std::function<void(int)> dfs = [&](int x) {
    for(auto && e: g[x]) {
      if(vis[e.first]) { continue; }
      vis[e.first] = 1;
      dfs(e.first);
    }
  };

  int ret = 0;
  rep(i, N) {
    if(!vis[i]) { vis[i] = 1; dfs(i); ret++; }
  }
  return ret;
}

int main() {

  int N, E, k; cin >> N >> E >> k;

  vector<vector<pair<int, int>>> g(N);

  rep(i, E) {
    int a, b, c; cin >> a >> b >> c;
    a--, b--;
    g[a].emplace_back(b, c);
    g[b].emplace_back(a, c);
  }

  cout << count_components(g) << endl;

  return 0;
}