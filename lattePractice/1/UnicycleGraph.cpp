#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

struct unicycle_graph {  // experimental

  // assume bidirectional graph

  int N;

  using graph_type = pair<int, int>;
  vector<vector<graph_type>> g; // to, cost;

  vector<int> cycle_vs_;
  vector<bool> in_cycle_v_;

  using edge = pair<int, int>;
  set<edge> in_cycle_edge_;

  unicycle_graph(int n) : N(n), g(n), in_cycle_v_(n) {}

  void add_edge(int f, int t, int c) {
    g[f].emplace_back(t, c);
    g[t].emplace_back(f, c);
  }

  void build() {
    vector<bool> vis(N);
    vector<int> prev(N);

    std::function<bool(int, int)> dfs = [&](int x, int p) {
      for(auto const& e: g[x]) {
        int to, cost; tie(to, cost) = e;
        if(to == p) { continue; }
        prev[to] = x;
        if(vis[to]) {
          for(int curr = x; curr != to; curr = prev[curr]) {
            cycle_vs_.push_back(curr);
            in_cycle_v_[curr] = 1;
            in_cycle_edge_.emplace(prev[curr], curr);
            in_cycle_edge_.emplace(curr, prev[curr]);
          }
          cycle_vs_.push_back(to);
          in_cycle_v_[to] = 1;
          reverse(all(cycle_vs_));
          return true;
        }
        else {
          vis[to] = 1;
          if(dfs(to, x)) { return true; }
        }
      }
      return false;
    };

    rep(i, N) {
      if(dfs(i, -1)) { break; }
    }
  }

  bool in_cycle(int x) { return in_cycle_v_[x]; }
  bool in_cycle(int f, int t) { return in_cycle_edge_.count({f, t}); }
  vector<int> const& cycle_vertices() { return cycle_vs_; }

};

int main() {

  int n, m; cin >> n >> m;
  unicycle_graph ug(n);
  rep(i, m) {
    int f, t, c; cin >> f >> t >> c; f--, t--;
    ug.add_edge(f, t, c);
  }

  ug.build();

  cout << endl;
  rep(i, n) {
    cout << ug.in_cycle(i) << endl;
  }

  cout << endl;

  for(auto && e: ug.cycle_vertices()) {
    cout << e << ", ";
  }cout << endl;

  return 0;
}