#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  int N, M, L; cin >> N >> M >> L;
  int dp[10101]; minus(dp);
  rep(i, N) {
    int a, b; cin >> a >> b;
    maximize(dp[a], b);
  }

  rep(i, M) {
    int c, d; cin >> c >> d;
    for(int j=10000; j>=0; j--) {
      if(j - c < 0) { continue; }
      if(dp[j - c] < 0) { continue; }
      maximize(dp[j], dp[j-c] + d);
    }
  }

  int ans = 0;

  rep(i, L+1) {
    maximize(ans, dp[i]);
  }

  cout << ans << endl;

  return 0;
}