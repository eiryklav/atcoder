#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
ll const inf = 1e18;

int count_components(vector<vector<pair<int, ll>>>& g) {

  int N = g.size();
  vector<bool> vis(N);
  std::function<void(int)> dfs = [&](int x) {
    for(auto && e: g[x]) {
      if(vis[e.first]) { continue; }
      vis[e.first] = 1;
      dfs(e.first);
    }
  };

  int ret = 0;
  rep(i, N) {
    if(!vis[i]) { vis[i] = 1; dfs(i); ret++; }
  }
  return ret;
}

struct unicycle_graph {
  
  // assumed bidirectional graph
  int N;

  using graph_type = pair<int, ll>;
  vector<vector<graph_type>> g; // to, cost;

  vector<int> cycle_vs_;
  vector<bool> in_cycle_v_;

  using edge = pair<int, int>;
  set<edge> in_cycle_edge_;

  unicycle_graph(int n) : N(n), g(n), in_cycle_v_(n) {}

  void add_edge(int f, int t, int c) {
    g[f].emplace_back(t, c);
    g[t].emplace_back(f, c);
  }

  void build() {
    vector<bool> vis(N);
    vector<int> prev(N);

    std::function<bool(int, int)> dfs = [&](int x, int p) {
      for(auto const& e: g[x]) {
        int to, cost; tie(to, cost) = e;
        if(to == p) { continue; }
        prev[to] = x;
        if(vis[to]) {
          for(int curr = x; curr != to; curr = prev[curr]) {
            cycle_vs_.push_back(curr);
            in_cycle_v_[curr] = 1;
            in_cycle_edge_.emplace(prev[curr], curr);
            in_cycle_edge_.emplace(curr, prev[curr]);
          }
          cycle_vs_.push_back(to);
          in_cycle_v_[to] = 1;
          in_cycle_edge_.emplace(to, prev[to]);
          in_cycle_edge_.emplace(prev[to], to);
          reverse(all(cycle_vs_));
          return true;
        }
        else {
          vis[to] = 1;
          if(dfs(to, x)) { return true; }
        }
      }
      return false;
    };

    rep(i, N) {
      if(!vis[i]) { vis[i] = 1; if(dfs(i, -1)) { break; } }
    }
  }

  bool in_cycle(int x) { return in_cycle_v_[x]; }
  bool in_cycle_edge(int f, int t) { return in_cycle_edge_.count({f, t}); }
  vector<int> const& cycle_vertices() { return cycle_vs_; }

};

int N, M, K;
vector<tuple<int, int, ll>> es;

ll solve(unicycle_graph& ug) {
  K -= count_components(ug.g);

  if(K <= 0) { return 0; }

  vector<ll> costs, cycle_costs;
  for(auto && e: es) {
    int f, t; ll c; tie(f, t, c) = e;
    if(ug.in_cycle_edge(f, t)) {
      cycle_costs.push_back(c);
    } else {
      costs.push_back(c);
    }
  }

  sort(all(costs));

  ll sum1 = inf;

  if(costs.size() >= K) {
    minimize(sum1, accumulate(costs.begin(), costs.begin() + K, 0));
    if(cycle_costs.empty()) { return sum1; }
  }

  sort(all(cycle_costs));
  ll sum2 = cycle_costs[0] + cycle_costs[1];
  REP(i, 2, cycle_costs.size()) { costs.push_back(cycle_costs[i]); }
  sort(all(costs));
  sum2 += accumulate(costs.begin(), costs.begin() + K-1, 0);

  return min(sum1, sum2);
}

int main() {

  cin >> N >> M >> K;
  unicycle_graph ug(N);
  rep(i, M) {
    int a, b; ll c; cin >> a >> b >> c;
    a --, b --;
    ug.add_edge(a, b, c);
    es.emplace_back(a, b, c);
  }

  ug.build();

  cout << solve(ug) << endl;

  return 0;
}