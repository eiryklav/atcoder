#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> using PQ_G = priority_queue<T, vector<T>, greater<T>>;

int A[100001], D[100001];

int main() {

  ll K, N; cin >> K >> N;
  rep(i, N) { cin >> A[i] >> D[i]; }
  ll ok = 1e12;  // compare(ok) is always true
  ll ng = 0;    // compare(ng) is always false

  auto compare = [&](ll threshold) {  // K回以上増築して増築王になる
    ll cnt = 0;
    rep(i, N) {
      if(threshold >= A[i]) { // 増築の結果、最小の建物の値段をthreshold以上まで引き上げる
        cnt += (threshold - A[i]) / D[i] + 1; // 増築可能な各々の建物について最大増築回数を求める
      }
    }
    return cnt >= K;
  };
  
  while(abs(ok-ng) > 1) { // ng極限の隣がokとなるまで
    ll mid = (ok + ng) / 2;
    (compare(mid) ? ok : ng) = mid;
  }

  ll res = 0;
  ll cnt = 0;
  rep(i, N) {
    if(ok >= A[i]) {
      ll num = (ok - A[i]) / D[i] + 1;
      cnt += num;
      res += (A[i] + (A[i] + (num - 1) * D[i])) * num / 2;
    }
  }

  res -= ok * (cnt - K);
  cout << res << endl;

  return 0;
}