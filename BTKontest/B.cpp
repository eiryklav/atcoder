#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef unsigned long long ll;
int const inf = 1<<29;

namespace math { namespace integer {

template<class value_type> value_type mod_pow(value_type x, value_type n, ll mod) { if(n == 0) { return 1; } if(n % 2 == 0) { return mod_pow(x * x % mod, n / 2, mod); } else { return x * mod_pow(x, n - 1, mod) % mod; } }
template<class value_type> value_type extgcd(value_type a, value_type b, value_type& x, value_type& y) { value_type d = a; if(b != 0) { d = extgcd(b, a%b, y, x); y -= (a / b) * x;} else { x = 1, y = 0; } return d; }
template<class value_type> value_type mod_inverse(value_type x, ll mod) { return mod_pow(x, value_type(mod-2), mod); /* use fermat */ }
template<class value_type> value_type mod_inverse_composite_num_mod(value_type a, ll mod) { value_type x, y; extgcd(a, mod, x, y); return (mod + x % mod) % mod; }

}}
using namespace math::integer;

ll const MOD = 1777777777;

namespace math {

struct CombinationHuge {

  using value_type = unsigned long long;

  // O(r)
  value_type comb(value_type n, int r) {
    value_type ret = 1;
    rep(i, r) {
      /*
      ret = mod_mul(ret, (value_type)(n - i), MOD);
      ret = mod_mul(ret, mod_inverse((value_type)i+1, MOD), MOD);
      */
      ret *= (value_type)(n - i); ret %= MOD;
      ret *= mod_inverse((value_type)i+1, MOD); ret %= MOD;
    }
    return ret;
  }

};

}
math::CombinationHuge chuge;

ll facts[1000000];

ll inclusive_exclusive_notation(ll K) {
  ll ret = 0;
  rep(i, K+1) {
    ll pat = mod_inverse(facts[i], MOD); pat %= MOD;
    if(i % 2 == 0) {
      ret += pat;
    } else {
      ret += MOD - pat;
    }
    ret %= MOD;
  }
  return (ret * facts[K]) % MOD;
}

void main_() {

  facts[0] = 1;
  REP(i, 1, 1000000) {
    facts[i] = (facts[i-1] * i) % MOD;  // 6 + 9 = 15 < 18
  }

  ll N, K; cin >> N >> K;
  cout << (chuge.comb(N, K) * inclusive_exclusive_notation(K)) % MOD << endl;
}