#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

struct xor128 {
  unsigned x,y,z,w;
  xor128(): x(std::random_device()()), y(std::random_device()()), z(std::random_device()()), w(std::random_device()()) {}
  unsigned next() {
    unsigned t=x^(x<<11);
    x=y;y=z;z=w;
    return w=w^(w>>19)^t^(t>>8);
  }
  unsigned next(unsigned k) {
    return next()%k;
  }
} rndgen;


template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

int G[30][30];
bool vis[30][30];

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};
int tesu;
int miss;
const int SaveLimit = 10;
queue<int> GS[30][30];

void debug_print_map() { rep(i, 30) rep(j, 30) { cout << G[i][j]; if(j == 29) cout << "\n"; else cout << " "; } }
void debug_save_map() { rep(i, 30) rep(j, 30) { GS[i][j].push(G[i][j]); if(GS[i][j].size() > SaveLimit) GS[i][j].pop(); } }
bool debug_print_pop_saved_map() { if(GS[0][0].empty()) { return false; } rep(i, 30) rep(j, 30) { cout << GS[i][j].front(); GS[i][j].pop(); if(j == 29) cout << "\n"; else cout << " "; } return true; }
void debug_print_state() { cout << "tesu: " << tesu << ", " << "miss: " << miss << endl; }
void debug_step_wait() { char c; cin >> c; }

bool output(int y, int x) {
  if(G[y][x] == 0) {
    miss++;
    return false;
  }
//  printf("%d %d\n", y+1, x+1);
  G[y][x] --;
  tesu ++;
  return true;
}

template<class T> using container = vector<T>;
using P = pair<int, int>;

container<P> ans;

vector<tuple<int, int, int>> g[30][30];


deque<P> dfs(int y, int x, const int X) {
  deque<pair<int, int>> ret;
  int msize = 0;
  for(auto && e: g[y][x]) {
    int ny, nx, width; tie(ny, nx, width) = e;
    if(!in_range(ny, nx, 30, 30)) { continue; }
    if(width > X) { continue; }
    auto r = dfs(ny, nx, X);
    if(msize < r.size()) {
      msize = r.size();
      ret = r;
    }
  }
  ret.emplace_front(y, x);
  return ret;
}

int solveLongestPath(int X, vector<P>& maxList) {
  int longest = 0;
  deque<P> path;
  for(auto& e: maxList) {
    auto r = dfs(e.first, e.second, X);
    if(r.size() > longest) {
      longest = r.size();
      path = r;
    }
  }

  int sum = 0;
  int max = G[path[0].first][path[0].second];
  ans.push_back(path[0]);
  REP(i, 1, path.size()) {
    int diff = max - G[path[i].first][path[i].second] - 1;
    rep(i, diff) {
      rep(j, i) { ans.push_back(path[j]); }
    }
    sum += diff;
    max = G[path[i].first][path[i].second];
  }
  reverse(all(path));
  REP(i, 1, path.size()) {
    G[path[i].first][path[i].second] = G[path[i-1].first][path[i-1].second] + 1;
  }
  return sum;
}

deque<pair<int, int>> greedy(int y, int x, int c) {
  deque<pair<int, int>> ret;
  int msize = 0;
  rep(i, 4) {
    int ny = y + dy[i], nx = x + dx[i];
    if(!in_range(ny, nx, 30, 30)) { continue; }
    if(G[ny][nx] != c+1) { continue; }
    auto r = greedy(ny, nx, c+1);
    if(msize < r.size()) {
      msize = r.size();
      ret = r;
    }
  }
  ret.emplace_front(y, x);
  return ret;
}

bool solve() {

  tesu = 0;
  // 浅瀬から切り崩す
  vector<int> mlist = {10, 1, 10};
  rep(_, inf) {
    for(auto && e: mlist) {
      // make graph
      rep(i, 30) rep(j, 30) g[i][j].clear();
      int max = 0;
      vector<P> maxList;
      rep(i, 30) rep(j, 30) {
        rep(k, 4) {
          int ni = i + dy[k], nj = j + dx[k];
          if(!in_range(ni, nj, 30, 30)) { continue; }
          if(G[i][j] <= G[ni][nj]) { continue; }  // 減少列
          g[i][j].emplace_back(ni, nj, G[ni][nj] - G[i][j] - 1);
          if(G[i][j] > max) {
            max = G[i][j];
            maxList = {{i, j}};
          } else if(G[i][j] == max) {
            maxList.emplace_back(i, j);
          }
        }
      }

      tesu += solveLongestPath(e, maxList);
    }

    int maxLongestPathLen = 0;
    deque<pair<int, int>> longestPath;  // 昇順
    rep(sy, 30) rep(sx, 30) {
      if(!G[sy][sx]) { continue; }
      auto path = greedy(sy, sx, G[sy][sx]);
      int len = path.size();
      if(maxLongestPathLen < len) {
        maxLongestPathLen = len;
        longestPath = path;
      }
    }

    if(maxLongestPathLen == 0) { break; }

    cout << 100000 - tesu << endl;

    reverse(all(longestPath)); // 降順化
    // 上から下る。
    tesu ++;
    for(auto && e: longestPath) {
      ans.push_back(e);
      assert(G[e.first][e.second] > 0);
      G[e.first][e.second] --;
    }
  }

  for(auto && e: ans) {
    printf("%d %d\n", e.first, e.second);
  }

  /*
  // 幅Xを三分探索
  int l = 1, r = 100;
  while(l + 1 < r) {
    int llm = (2 * l + r) / 3;
    int rrm = (l + 2 * r) / 3;
    container<P> outs1, outs2;
    int fl = dijkstra(llm, outs1);
    int fr = dijkstra(rrm, outs2);
    if(fl >= fr) {
      l = llm;
      ans = outs1;
    } else {
      r = rrm;
    }
  }
  for(auto && e: ans) {
    printf("%d %d\n", e.first, e.second);
  }
  */

  return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void debug_map_gen() {
  rep(i, 30) rep(j, 30) {
    G[i][j] = rndgen.next(100) + 1;
  }
}

int main() {

  #if 0
  rep(i, 30) rep(j, 30) { cin >> G[i][j]; }
  #else
  debug_map_gen();
  #endif
  while(solve()) {
    #if 0
    debug_print_map();
    char c; cin >> c;
    #endif
  }

  #if 1
  cout << "---------------------------------------------\n";
  rep(i, SaveLimit) debug_print_pop_saved_map(), cout << "---------------------------------------------\n";
  cout << 100000 - tesu << "::" << endl;
  cout << "miss: " << miss << endl;
  #endif

  return 0;
}