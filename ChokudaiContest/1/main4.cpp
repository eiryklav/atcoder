#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> constexpr bool in_range_real(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }
template<class T> constexpr bool in_range(T y, T x, T H, T W) { return -2*H-1<=y&&y<H&&0<=x&&x<W; }

int G[30][30];
bool vis[30][30];

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};
int tesu;
int miss;

void debug_print_map() { rep(i, 30) rep(j, 30) { cout << G[i][j]; if(j == 29) cout << "\n"; else cout << " "; } }
void debug_print_state() { cout << "tesu: " << tesu << ", " << "miss: " << miss << endl; }
void debug_step_wait() { char c; cin >> c; }

bool output(int y, int x) {
  if(!in_range_real(y, x, 30, 30)) { return false; }
  if(G[y][x] == 0) {
    miss++;
    return false;
  }
  printf("%d %d\n", y+1, x+1);
  G[y][x] --;
  tesu ++;
  return true;
}

bool solve() {

  int sy = 29, sx = 29;
  rep(_, 1000) {
    int y = sy, x = sx;
    output(y, x);

    rep(__, 1000){
      while(in_range(y+1, x-1, 30, 30)) {
        y ++, x --;
        output(y, x);
      }

      if(in_range(y, x+1, 30, 30)) {
        x ++;
        output(y, x);
      }

      while(in_range(y-1, x+1, 30, 30)) {
        y --, x ++;
        output(y, x);
      }

      if(in_range(y+1, x, 30, 30)) {
        y ++;
        output(y, x);
      }
    }
    if(in_range(y, x+1, 30, 30)) { x ++; output(y, x); }
    if(in_range(y+1, x, 30, 30)) { y ++; output(y, x); }

    cout << ":" << y << ", " << x << endl;
    if(!(y == 29 && x == 29)) {
      debug_print_map();
      break;
    }
    sy --;
  }

  return false;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct xor128 {
  unsigned x,y,z,w;
  xor128(): x(std::random_device()()), y(std::random_device()()), z(std::random_device()()), w(std::random_device()()) {}
  unsigned next() {
    unsigned t=x^(x<<11);
    x=y;y=z;z=w;
    return w=w^(w>>19)^t^(t>>8);
  }
  unsigned next(unsigned k) {
    return next()%k;
  }
} rndgen;

void debug_map_gen() {
  rep(i, 30) rep(j, 30) {
    G[i][j] = rndgen.next(100) + 1;
  }
}

int main() {

  #if 0
  rep(i, 30) rep(j, 30) { cin >> G[i][j]; }
  #else
  debug_map_gen();
  #endif
  while(solve()) {
    #if 0
    debug_print_map();
    char c; cin >> c;
    #endif
  }

  #if 1
  cout << "---------------------------------------------\n";
  cout << 100000 - tesu << "::" << endl;
  cout << "miss: " << miss << endl;
  #endif

  return 0;
}