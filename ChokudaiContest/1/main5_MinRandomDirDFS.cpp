#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

int G[30][30];
bool vis[30][30];

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};
int tesu;
int miss;
const int SaveLimit = 10;
queue<int> GS[30][30];

void debug_print_map() { rep(i, 30) rep(j, 30) { cout << G[i][j]; if(j == 29) cout << "\n"; else cout << " "; } }
void debug_save_map() { rep(i, 30) rep(j, 30) { GS[i][j].push(G[i][j]); if(GS[i][j].size() > SaveLimit) GS[i][j].pop(); } }
bool debug_print_pop_saved_map() { if(GS[0][0].empty()) { return false; } rep(i, 30) rep(j, 30) { cout << GS[i][j].front(); GS[i][j].pop(); if(j == 29) cout << "\n"; else cout << " "; } return true; }
void debug_print_state() { cout << "tesu: " << tesu << ", " << "miss: " << miss << endl; }
void debug_step_wait() { char c; cin >> c; }

bool output(int y, int x) {
  if(G[y][x] == 0) {
    miss++;
    return false;
  }
  //printf("%d %d\n", y+1, x+1);
  G[y][x] --;
  tesu ++;
  return true;
}

bool solve() {

  // Min... は 1の量が増加していくので不可！

  while(1) {
    int i, j, c = inf;  // position に ランダム性?
    rep(a, 30) rep(b, 30) {
      if(G[a][b] && c > G[a][b]) {
        c = G[a][b];
        i = a, j = b;
      }
    }
    if(c == inf) { break; }
    output(i, j); c ++;
    bool next = 1;
    while(next) {
      next = 0;
      rep(k, 4) {
        int ni = i + dy[k], nj = j + dx[k];
        if(!in_range(ni, nj, 30, 30)) { continue; }
        if(c && c == G[ni][nj]) {
          next = 1;
          i = ni, j = nj;
          if(output(i, j)) { tesu --; } c ++;
          break;
        }
      }
//      if(abs(MaxC - c) < 20) { break; }
      if(!next) { break; }
    }
//    debug_save_map();
//    debug_print_map();
//    debug_print_state();
//    debug_step_wait();
  }
  return false;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct xor128 {
  unsigned x,y,z,w;
  xor128(): x(std::random_device()()), y(std::random_device()()), z(std::random_device()()), w(std::random_device()()) {}
  unsigned next() {
    unsigned t=x^(x<<11);
    x=y;y=z;z=w;
    return w=w^(w>>19)^t^(t>>8);
  }
  unsigned next(unsigned k) {
    return next()%k;
  }
} rndgen;

void debug_map_gen() {
  rep(i, 30) rep(j, 30) {
    G[i][j] = rndgen.next(100) + 1;
  }
}

int main() {

  #if 0
  rep(i, 30) rep(j, 30) { cin >> G[i][j]; }
  #else
  debug_map_gen();
  #endif
  while(solve()) {
    #if 0
    debug_print_map();
    char c; cin >> c;
    #endif
  }

  #if 1
  cout << "---------------------------------------------\n";
  rep(i, SaveLimit) debug_print_pop_saved_map(), cout << "---------------------------------------------\n";
  cout << 100000 - tesu << "::" << endl;
  cout << "miss: " << miss << endl;
  #endif

  return 0;
}