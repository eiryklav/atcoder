#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }


int G[30][30];
bool vis[30][30];

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};
int tesu;

bool output(int y, int x) {
  if(G[y][x] == 0) { return false; }
  printf("%d %d\n", y+1, x+1);
  G[y][x] --;
  tesu ++;
  return true;
}

bool narashi() {
  bool ret = 0;
  rep(ii, 14) {
    rep(i, 30) {
      ret |= output(ii*2, i);
    }
    rep(i, 30) {
      ret |= output(ii*2+1, 29-i);
    }
  }
  rep(i, 30) {
    ret |= output(29, i);
  }
  rep(i, 30) {
    ret |= output(29, 29-i);
  }
  rep(ii, 14) {
    rep(i, 30) {
      ret |= output(28-(ii*2), i);
    }
    rep(i, 30) {
      ret |= output(28-(ii*2+1), 29-i);
    }
  }
  return ret;
}

struct xor128 {
  unsigned x,y,z,w;
  xor128(): x(std::random_device()()), y(std::random_device()()), z(std::random_device()()), w(std::random_device()()) {}
  unsigned next() {
    unsigned t=x^(x<<11);
    x=y;y=z;z=w;
    return w=w^(w>>19)^t^(t>>8);
  }
  unsigned next(unsigned k) {
    return next()%k;
  }
} rndgen;

void debug_map_gen() {
  rep(i, 30) rep(j, 30) {
    G[i][j] = rndgen.next(100) + 1;
  }
}

void debug_print_map() {
  rep(i, 30) rep(j, 30) { cout << G[i][j]; if(j == 29) cout << "\n"; else cout << " "; }
}

int main() {

  #if 1
  rep(i, 30) rep(j, 30) { cin >> G[i][j]; }
  #else
  debug_map_gen();
  #endif
  while(narashi()) {
    #if 0
    debug_print_map();
    char c; cin >> c;
    #endif
  }
//  cout << 100000 - tesu << "::" << endl;

  return 0;
}