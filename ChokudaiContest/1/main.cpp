#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

void output(int y, int x) { printf("%d %d\n", x, y); }

int G[30][30];
bool vis[30][30];

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int main() {

  rep(i, 30) rep(j, 30) {
    cin >> G[i][j];
  }

  int dir = 3;
  int y = 0, x = 0;
  output(y, x);
  vis[0][0] = 1;
  while(1) {
    int ny = y + dy[dir], nx = x + dx[dir];
    if(!in_range(ny, nx, 30, 30)) {
      bool ok = 1;
      rep(i, 4) {
        ny = y + dy[i], nx = x + dx[i];
        if(in_range(ny, nx, 30, 30)) {
          ok = 1;
          dir = i;
          break;
        }
      }
      if(!ok) {
        zero(vis);
        vis[y][x] = 1;
        output(y, x);
        continue;
      }
      
      y = ny, x = nx;
      vis[y][x] = 1;
      output(y, x);
    }

    y = ny, x = nx;
    vis[y][x] = 1;
    output(y, x);

  }

  return 0;
}