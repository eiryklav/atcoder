#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int main() {

  int N, M; cin >> N >> M;
  int s, t; cin >> s >> t; s--, t--;

  int a[1001][1001];
  rep(i, 1001) rep(j, 1001) { a[i][j] = inf; if(i==j) { a[i][i] = 0; } }

  rep(i, M) {
    int x, y, d; cin >> x >> y >> d;
    x--, y--;
    a[x][y] = a[y][x] = d;
  }

  rep(k, N) rep(i, N) rep(j, N) {
    minimize(a[i][j], a[i][k] + a[k][j]);
  }

  int min = inf;
  int ans = -1;
  rep(k, N) {
    if(a[s][k] == a[k][t]) {
      if(min > a[s][k]) {
        min = a[s][k];
        ans = k + 1;
      }
    }
  }

  cout << ans << endl;

  return 0;
}