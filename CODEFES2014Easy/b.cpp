#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int main() {

  int a[202];
  int curr = 1;
  int dir = -1;
  REP(i, 1, 202) {
    a[i] = curr;
    if(curr >= 20 || curr <= 1) {
      dir *= -1;
    }
    curr += dir;
    if(curr == 20) a[i+1] = 20, i++;
    if(curr == 1 && dir < 0) a[i+1] = 1, i++;
  }

  int n; cin >> n;
  cout << a[n] << endl;

  return 0;
}