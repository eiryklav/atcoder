#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

struct union_find {
  vector<int> rank_, size_, rid_;
  union_find(int n) { rank_.resize(n); rid_.assign(n, -1); size_.resize(n, 1); }
  void operator()(int u, int v) {
    u = operator[](u), v = operator[](v);
    if(u == v) { return; }
    size_[u] = size_[v] = size_[u] + size_[v];
    if(rank_[u] < rank_[v]) { rid_[u] = v; }
    else { rid_[v] = u; if(rank_[u] == rank_[v]) { rank_[u]++; } }    
  }
  int operator[](int x) { if(rid_[x] < 0) return x; else return rid_[x] = operator[](rid_[x]); }
  int size_of(int x) { return size_[x]; }
};

void main_() {

  int N, M, Q; cin >> N >> M >> Q;
  union_find uf(N+M);
  rep(i, Q) {
    int c, d; cin >> c >> d;
    uf(c-1, N+d-1);
  }

  int c = 0, d = 0;
  int ans = 0;
  for(int i=0; i<N/__gcd(N, M)*M; i++) {
    if(uf[c] != uf[N+d]) {
      ans = i+1;
      uf(c, N+d);
    }
    c ++, d ++;
    if(c >= N) { c = 0; }
    if(d >= M) { d = 0; }
  }

  cout << ans << endl;

}