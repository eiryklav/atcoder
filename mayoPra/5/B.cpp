#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

namespace math { namespace prime {

constexpr int Max = 10000001;

int prime[Max];
bool is_prime[Max];

int sieve(int n) {
  int prime_count = 0;
  rep(i, n+1) is_prime[i] = 1;
  is_prime[0] = is_prime[1] = 0;
  REP(i, 2, n+1) {
    if(!is_prime[i]) { continue; }
    prime[prime_count++] = i;
    for(long long j=(long long)i*i; j<=n; j+=i) {
      is_prime[j] = 0;
    }
  }
  return prime_count;
}

#define USE_PRIME_SIEVE \
  using math::prime::prime; \
  using math::prime::is_prime;  \
  using math::prime::sieve;
}}

USE_PRIME_SIEVE

void main_() {
  
  sieve(10000000);
  int ans = 0;
  int aidx = -1;
  REP(q, -2000, 2001) {
    int cand = 0;
    REP(n, 1, 101) {
      int C = n*n+n+q-(n/96-n/97)*9351;
      if(q == 41 && !is_prime[C]) {
        cout << n << ", " << C << endl;
      }
      if(C < 0) { continue; }
      cand += is_prime[C];
    }
    if(ans < cand) {
      ans = cand;
      aidx = q;
    }
  }

  cout << aidx << ", " << ans << endl;

}