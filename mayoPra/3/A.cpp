#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

ll MOD;

namespace Matrix
{
  using number_type = ll;
  using Vec = vector<number_type>;
  using Mat = vector<Vec>;

  Mat operator * (Mat& A, Mat& B) {
    Mat C(A.size(), Vec(B[0].size()));
    rep(i, A.size()) rep(k, B.size()) rep(j, B[0].size())
      C[i][j] = (C[i][j] + A[i][k] * B[k][j]) % MOD;
    return C;
  }

  void operator *= (Mat& A, Mat& B) {
    A = A*B;
  }

  Mat pow(Mat A, ll n) {
    Mat B(A.size(), Vec(A.size()));
    rep(i, A.size()) {
      B[i][i] = 1;
    }
    while(n > 0) {
      if(n & 1) { B *= A; }
      A *= A;
      n >>= 1;
    }
    return std::move(B);
  }

  Mat operator ^ (Mat A, ll n) { return std::move(pow(std::move(A), n)); }

  Mat make_matrix(number_type a, number_type b, number_type c, number_type d) {
    Mat ret(2, Vec(2));
    ret[0][0] = a, ret[0][1] = b,
    ret[1][0] = c, ret[1][1] = d;
    return std::move(ret);
  }
}

using namespace Matrix;

void main_() {
  int N; cin >> N;
  ll X, T, A, B, C; cin >> X >> T >> A >> B >> C;
  MOD = C;
  auto m = make_matrix(
    A, B,
    0, 1
  );
  m = m ^ T;
  A = m[0][0], B = m[0][1];
  ll res = 0;
  rep(i, N) {
    res += X;
    X = (A * X + B) % C;
  }
  cout << res << endl;
}