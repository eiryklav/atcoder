#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

constexpr int MOD = 1e9+7;

namespace math {

constexpr int MaxComb = 1000;

struct Combination {

  long long comb_[MaxComb][MaxComb];
    
  Combination() {
    rep(i, MaxComb) {
      comb_[i][0] = 1;
      REP(j, 1, i+1) {
        comb_[i][j] = comb_[i-1][j-1] + comb_[i-1][j];
        comb_[i][j] %= MOD;
      }
    }
  }

  long long comb(int n, int r) const { return comb_[n][r]; }

};

}

math::Combination c;

int R, C;
int X, Y;
int D, L;

void main_() {

  cin >> R >> C;
  cin >> X >> Y;
  cin >> D >> L;

  ll res = 0;

  rep(mask, 1<<4) {
    int w = X, h = Y;
    int parity = 0;
    if(mask >> 0 & 1) {
      w --;
      parity ++;
    }
    if(mask >> 1 & 1) {
      w --;
      parity ++;
    }
    if(mask >> 2 & 1) {
      h --;
      parity ++;
    }
    if(mask >> 3 & 1) {
      h --;
      parity ++;
    }
    if(!(w + 1) || !(h + 1)) { continue; }
    if(w * h < D + L) { continue; }
    res = res + c.comb(w * h, D) % MOD * c.comb(w * h - D, L) * (parity % 2 ? -1 : +1);
    res %= MOD; res += MOD; res %= MOD;
  }
  res *= R - X + 1;
  res %= MOD;
  res *= C - Y + 1;
  res %= MOD;
  cout << res << endl;
}