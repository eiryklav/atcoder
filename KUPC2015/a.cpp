#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int T; cin >> T;
  rep(_, T) {
    string s; cin >> s;
    int N = s.size();
    if(N < 5) { cout << 0 << endl; continue; }
    int cnt = 0;
    rep(i, N-4) {
      if(s.substr(i, 5) == "tokyo") {
        cnt ++;
        s[i] = 'x';
        s[i+4] = 'x';
      }
      else if(s.substr(i, 5) == "kyoto") {
        cnt ++;
        s[i] = 'x';
        s[i+4] = 'x';
      }
    }
    cout << cnt << endl;
  }

  return 0;
}