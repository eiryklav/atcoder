#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int constexpr Max = 1e5+1;

int N;
ll A[Max], B[Max];
ll reqsum[Max];
ll req[Max];

pair<ll, int> dp[Max];  // i地点まで最短 := 貯蓄最大, 直近の最適な溜める場所

int main() {

  cin >> N;
  rep(i, N) cin >> A[i];
  rep(i, N) {
    cin >> B[i];
    if(i) reqsum[i] = reqsum[i-1];
    reqsum[i] += B[i];
    req[i] = min(0LL, A[i]);
  }

  rep(i, N) {
    cout << req[i] << endl;
  }
  exit(0);

  ll ans = 0;//, sum = 0;
  rep(i, N) {
    // i-1日まで最短
    if(i && B[dp[i-1].second] > 0) {
      int tame_day = req[i] / B[dp[i-1].second] + req[i] % dp[i-1].second > 0;
      dp[i] = {dp[i-1].first + tame_day * B[i], dp[i-1].second};
//      dp[i] = {dp[i-1].first + req[i] / B[dp[i-1].second], dp[i-1].second};
    }
    if(B[i-1] > 0) {
      dp[i] = max(dp[i], {dp[i-1].first + req[i] / B[i-1], i-1});
    }

    // i日で滞在をmaxとる
    ans = max(ans, dp[i].first + (N-i) * B[i]);
  }

  cout << ans << endl;

  return 0;
}