#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int G[33][33];
int V[33][33];

int main() {

  int T; cin >> T;
  rep(_, T) {
    int N; cin >> N;
    rep(i, N) rep(j, N) {
      cin >> G[i][j];
      V[i][j] = G[i][j];
    }

    rep(k, N) rep(i, N) rep(j, N) {
      if(G[i][k] != -1 && G[k][j] != -1) {
        G[i][j] = min(G[i][j], G[i][k] + G[k][j]);
      }
    }

    rep(i, N) rep(j, N) {
      if(V[i][j] != G[i][j]) {
        goto ng;
      }
    }

    rep(i, N) {
      if(V[i][i] != 0) { goto ng; }
    }

    rep(k, N) rep(i, N) rep(j, N) {
      if(V[i][j] == -1 &&
        V[i][k] >= 0 && V[k][j] >= 0) {
        goto ng;
      }
    }

    cout << "YES\n"; continue;

    ng:;
    cout << "NO\n";
  }

  return 0;
}