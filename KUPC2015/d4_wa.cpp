#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int constexpr Max = 1e5+2;

int N;
ll A[Max], B[Max];

pair<ll, int> dp[Max];  // i地点まで最短 := 貯蓄最大, 直近の最適な溜める場所

int main() {

  cin >> N;
  // iに「入る」ためのコスト変化
  rep(i, N-1) cin >> A[i+1]; int x; cin >> x;

  rep(i, N) {
    cin >> B[i];
  }

  REP(i, 1, N) {
    dp[i].first = -1e18;
  }

  ll ans = 0;

  int M = N;// + 1;
  rep(i, M) {

    // i地点まで最短

    if(i && A[i] < 0 && dp[i-1].first + A[i] < 0 && B[dp[i-1].second] > 0) {
      // A[i]を通過するために余分に溜める必要がある
      // 進むために溜めてNを消費してよい理由は、もう一切進まなくて最適が逐次出ているため。
      // 後ろのほうで滞在するなら、少なくとも最大効率の最小の貯めは必要。

      int req = -A[i];
      int const LastMaxB = B[dp[i-1].second];
      int tame_day = req / LastMaxB + req % LastMaxB > 0;

      if(tame_day <= N) {

        N -= tame_day;

        int nextP = dp[i-1].first + A[i] + tame_day * LastMaxB;
        dp[i] = {nextP, dp[i-1].second};

//        cout << i << " " << dp[i].first << ' ' << dp[i].second << endl;
//        cout << req << ' ' << LastMaxB << endl << endl;
      }
      else {
        // 貯められない = 通過できない = 解は既に出ている
      }

    }
    else {
      // 余分に貯める必要はない

      if(i && dp[i-1].first + A[i] >= 0) {
        // 通過できるから
        int nextP = dp[i-1].first + A[i];
        dp[i] = {nextP, dp[i-1].second};
      }
      else {
        /*
        // はじめに留まる。 通過できない(解は既に出ている)
        if(i) dp[i].first = dp[i-1].first;
        else  dp[i].first = 0;
        */
      }
    }

    // MaxBのindexを更新
    if(B[dp[i].second] < B[i]) dp[i].second = i;

    // i日で滞在をmaxとる or 貯め場で消費しておいて、i日まで進む
    ans = max(ans, dp[i].first + (N-i) * B[dp[i].second]);

//    cout << "ansmax: " << ans << ", remain: " << N-i << " days" << endl;
    
  }

  cout << ans << endl;

  return 0;
}