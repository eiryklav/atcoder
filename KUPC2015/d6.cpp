#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int constexpr Max = 1e5+2;

ll N;
ll A[Max], B[Max];

pair<ll, ll> dp[Max];  // i地点まで最短 := 貯蓄最大, 直近の最適な溜める場所

int main() {

  cin >> N;
  // iに「入る」ためのコスト変化
  rep(i, N) cin >> A[i+1];

  rep(i, N) {
    cin >> B[i];
  }

  REP(i, 1, N) {
    dp[i].first = -1e18;
  }

  ll ans = 0;

  ll M = N + 1;
  rep(i, M) {

    if(i && A[i] < 0 && dp[i-1].first + A[i] < 0 && B[dp[i-1].second] > 0) {
      ll req = -A[i];
      ll const LastMaxB = B[dp[i-1].second];
      ll tame_day = req / LastMaxB + (req % LastMaxB > 0);

      if(tame_day <= N-1) {

        N -= tame_day;

        ll nextP = dp[i-1].first + A[i] + tame_day * LastMaxB;
//        cout << dp[i-1].first << ' ' << A[i] << ' ' << tame_day << ' ' << LastMaxB << endl;
//        assert(nextP >= 0);
        dp[i] = {nextP, dp[i-1].second};
      }
      else {

      }
    }
    else {
      if(i && dp[i-1].first + A[i] >= 0) {
        // 通過できるから
        ll nextP = dp[i-1].first + A[i];
        dp[i] = {nextP, dp[i-1].second};
      }
      else {
      }
    }

    if(B[dp[i].second] < B[i]) dp[i].second = i;

    // i日で滞在をmaxとる or 貯め場で消費しておいて、i日まで進む
    ans = max(ans, dp[i].first + (N-i) * B[dp[i].second]);
//    cout << "ansmax:" << ans << endl;
  }

  cout << ans << endl;

  return 0;
}