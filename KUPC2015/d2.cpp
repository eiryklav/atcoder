#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int constexpr Max = 1e5+1;

int N;
ll A[Max], B[Max];

pair<ll, int> dp[Max];  // i地点まで最短 := 貯蓄最大, 直近の最適な溜める場所

int main() {

  cin >> N;
  rep(i, N) cin >> A[i];
  rep(i, N) {
    cin >> B[i];
  }

  ll ans = 0;
  rep(i, N) {

    // i-1日まで最短

    if(i && A[i] < 0 && dp[i-1].first + A[i] < 0 && B[dp[i-1].second] > 0) {
      // A[i]を通過するために余分に溜める必要がある
      int const LastMaxB = B[dp[i-1].second];
      int req = -A[i];
      int tame_day = req / LastMaxB + req % LastMaxB > 0;
      int nextP = dp[i-1].first + A[i] + tame_day * LastMaxB;
      dp[i] = {nextP, dp[i-1].second};
    }
    else {
      if(i && dp[i-1].first + A[i] >= 0) {
        // 移動するだけでよい
        int nextP = dp[i-1].first + A[i];
        dp[i] = {nextP, dp[i-1].second};
      }
      else {
        // 移動できない
        if(i) dp[i].first = dp[i-1].first;
        else  dp[i].first = 0;
      }
    }

    // i日で滞在をmaxとる
    ans = max(ans, dp[i].first + (N-i) * B[i]);
    
  }

  cout << ans << endl;

  return 0;
}