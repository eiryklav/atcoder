#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

namespace flow {

struct edge {
  int to, cap, cost, rev;
  edge(int t, int ca, int co, int r) : to(t), cap(ca), cost(co), rev(r) {}
};

struct primal_dual {

  vector<vector<edge>> G;
  vector<int> h;
  vector<int> dist;
  vector<int> prevv, preve;

  primal_dual(int V) : G(V), h(V), dist(V), prevv(V), preve(V) {}

  void add_edge(int from, int to, int cap, int cost) {
    G[from].emplace_back(to, cap, cost, G[to].size());
    G[to].emplace_back(from, 0, -cost, G[from].size()-1);
  }

  int min_cost_flow(int s, int t, int f) {
    int ret = 0;
    fill(h.begin(), h.end(), 0);
    while(f > 0) {
      typedef pair<int, int> P;
      priority_queue<P, vector<P>, greater<P>> pq;
      fill(dist.begin(), dist.end(), inf);
      dist[s] = 0;
      pq.emplace(0, s);
      while(!pq.empty()) {
        P p = pq.top(); pq.pop();
        int v = p.second;
        if(dist[v] < p.first) { continue; }
        for(int i=0; i<G[v].size(); i++) {
          auto& e = G[v][i];
          if(e.cap > 0 && dist[e.to] > dist[v] + e.cost + h[v] - h[e.to]) {
            dist[e.to] = dist[v] + e.cost + h[v] - h[e.to];
            prevv[e.to] = v;
            preve[e.to] = i;
            pq.emplace(dist[e.to], e.to);
          }
        }
      }

      if(dist[t] == inf) { return -1; }
      for(int i=0; i<h.size(); i++) h[i] += dist[i];

      int d = f;
      for(int i=t; i!=s; i=prevv[i]) {
        d = min(d, G[prevv[i]][preve[i]].cap);
      }
      f -= d;
      ret += d * h[t];
      for(int i=t; i!=s; i=prevv[i]) {
        auto& e = G[prevv[i]][preve[i]];
        e.cap -= d;
        G[i][e.rev].cap += d;
      }
    }
    return ret;
  }

};

}


void main_() {
  
  flow::primal_dual pd(2222);
  int R, G, B; cin >> R >> G >> B;
  int src = 2000, dst = 2221;
  int rsrc = 2001, gsrc = 2002, bsrc = 2003;
  pd.add_edge(src, rsrc, R, 0);
  pd.add_edge(src, gsrc, G, 0);
  pd.add_edge(src, bsrc, B, 0);

  for(int i=-777; i<=777; i++) {
    pd.add_edge(rsrc, i+777, 1, abs(i+100));
    pd.add_edge(gsrc, i+777, 1, abs(i));
    pd.add_edge(bsrc, i+777, 1, abs(i-100));
    pd.add_edge(i+777, dst, 1, 0);
  }

  cout << pd.min_cost_flow(src, dst, R + G + B) << endl;

}
