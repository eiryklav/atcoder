#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  int N, K; cin >> N >> K;
  ll w[N]; double p[N];
  rep(i, N) cin >> w[i] >> p[i], p[i] /= 100.0;
  double l = 0.0, r = 100.0;
  rep(i, 100) {
    double m = (l + r) / 2.0;
    vector<double> d(N);
    rep(i, N) d[i] = w[i] * p[i] - w[i] * m;
    sort(all(d), greater<double>());
    double sum = 0.0;
    rep(i, K) {
      sum += d[i];
    }
    if(sum >= 0) {
      l = m;
    } else {
      r = m;
    }
  }

  printf("%.10f\n", (l + r) / 0.02);

  return 0;
}