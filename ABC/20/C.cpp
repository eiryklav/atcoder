#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int H, W, T;
vector<string> G;
int sx, sy, gx, gy;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

int const INF = 1e9+1;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

inline bool valid(int x, int y) {
  return 0<=x&&x<W && 0<=y&&y<H;
}

bool dijkstra(int X) {
  
  vector<vector<int>> dist(H, vector<int>(W, INF));
  
  priority_queue<pair<int, pair<int, int>>> pq;
  pq.push({0, {sx, sy}});
  dist[sy][sx] = 0;
  while(!pq.empty()) {
    auto top = pq.top();
    int cost = -top.first;
    int x = top.second.first;
    int y = top.second.second;
    pq.pop();

    if(gx == x && gy == y) {
      return dist[y][x] <= T;
    }

    rep(k, 4) {
      int nx = x+dx[k], ny = y+dy[k];
      if(!valid(nx, ny)) { continue; }
      if(G[ny][nx] == '#') {
        if(cost > T-X) { continue; }
      }
      int ncost = cost+(G[ny][nx]=='#'?X:1);
      if(dist[ny][nx] <= ncost) { continue; }

      dist[ny][nx] = ncost;
      pq.push({-ncost,{nx,ny}});
    }
    
  }

  return false;
}

int main() {

  cin >> H >> W >> T;
  G.resize(H);

  rep(i, H) cin >> G[i];
  rep(i, H) rep(j, W) {
    if(G[i][j] == 'S') {
      sx = j, sy = i;
    }
    if(G[i][j] == 'G') {
      gx = j, gy = i;
    }
  }

  int L = 0, R = 1e9+1;
  while(R-L>1) {
    int M = (R+L)/2;
    if(dijkstra(M)) {
      L = M;
    }
    else {
      R = M;
    }
  }

  cout << L << endl;
    
  return 0;
}
