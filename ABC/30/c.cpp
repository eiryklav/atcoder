#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int N, M; cin >> N >> M;
  int X, Y; cin >> X >> Y;

//  vector<int> a(N), b(M);
  priority_queue<pair<int, bool>, vector<pair<int, bool>>, greater<pair<int, bool>>> pq;
  int a, b;
  rep(i, N) cin >> a, pq.emplace(a, false);
  rep(i, M) cin >> b, pq.emplace(b, true);

  int nextTime = 0;
  bool nextB = false;
  int res = 0;
  while(!pq.empty()) {
    auto p = pq.top(); pq.pop();
    if(p.second == nextB) {
      if(nextTime <= p.first) {
        nextB = !nextB;
        if(!nextB) { res++; nextTime = p.first + Y; }
        else { nextTime = p.first + X; }
      }
    }
  }

  cout << res << endl;

  return 0;
}