#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  double a, b, c, d; cin >> a >> b >> c >> d;
  vector<string> v = {"TAKAHASHI", "AOKI"};
  if(abs(b/a - d/c) < 1e-7) { cout << "DRAW\n"; return 0; }
  cout << v[(b/a < d/c)] << endl;

  return 0;
}