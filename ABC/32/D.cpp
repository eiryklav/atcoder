#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template <class T>
struct knapsack {
  T c; 
  struct item { T p, w; }; // price/weight
  vector<item> is;
  void add_item(T p, T w) {
    is.push_back({p, w});
  }
  T det(T a, T b, T c, T d) {
    return a * d - b * c;
  }
  T z;
  void expbranch(T p, T w, int s, int t) {
    if (w <= c) {
      if (p >= z) z = p;
      for (; t < is.size(); ++t) {
        if (det(p - z - 1, w - c, is[t].p, is[t].w) < 0) return;
        expbranch(p + is[t].p, w + is[t].w, s, t + 1);
      }
    } else {
      for (; s >= 0; --s) {
        if (det(p - z - 1, w - c, is[s].p, is[s].w) < 0) return;
        expbranch(p - is[s].p, w - is[s].w, s - 1, t);
      }
    }
  }
  T solve() {
    sort(all(is), [](const item &a, const item &b) { 
      return a.p * b.w > a.w * b.p;
    });
    T p = 0, w = 0;
    z = 0;
    int b = 0; 
    for (; b < is.size() && w <= c; ++b) {
      p += is[b].p;
      w += is[b].w;
    }
    expbranch(p, w, b-1, b);
    return z;
  }
};

template<class T>
struct Knapsack {
  typedef pair<T, T> data;  // weight, value
  vector<data> sack;
  T weightLimits;
  Knapsack(T wlim) : weightLimits(wlim) {}

  void add(T weight, T value) { sack.emplace_back(weight, value); }

  T branching_bounding(T weight, T value, int s, int t) {
    
  }

};

int main() {

  return 0;
}