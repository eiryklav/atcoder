#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  int N, K; cin >> N >> K;
  ll curr = 1;
  int l = 0;
  vector<int> S(N);
  int ans = 0;
  rep(i, N) {
    cin >> S[i];
    if(S[i] == 0) { cout << N << endl; return 0; }
    curr *= S[i];
    while(curr > K && curr > 0) {
      curr /= S[l++];
      if(l > i) { break; }
    }
    if(curr <= K && curr > 0) {
      maximize(ans, i + 1 - l);
    }
  }

  cout << ans << endl;

  return 0;
}