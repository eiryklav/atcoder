#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int n ;cin >> n;
  int a[] = {60, 90, 100};
  string b[] = {"Bad", "Good", "Great", "Perfect"};

  rep(i, 3) {
    if(n < a[i]) {
      cout << b[i] << endl;
      exit(0);
    }
  }

  cout << b[3] << endl;

  return 0;
}
