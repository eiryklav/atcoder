#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  string s; cin >> s;
  int m[128] = {};
  rep(i, s.size()) {
    m[s[i]]++;
  }

  cout << m['A'];
  rep(i, 5) {
    cout << ' ' << m['B'+i];
  }
  cout << endl;

  return 0;
}