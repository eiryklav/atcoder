#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int a[5]; rep(i, 5) cin >> a[i];

  vector<int> v;
  rep(i, 5) REP(j, i+1, 5) REP(k, j+1, 5) {
    v.push_back(a[i]+a[j]+a[k]);
  }

//    sort(all(v));
  sort(v.begin(), v.end());
  reverse(v.begin(), v.end());
  v.erase(unique(v.begin(),v.end()), v.end());

  cout << v[2] << endl;

  return 0;
}