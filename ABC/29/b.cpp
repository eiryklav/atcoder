#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int ans = 0;
  rep(i, 12) {
    string s;
    cin >> s;
    rep(i, s.size()) {
      if(s[i] == 'r') {
        ans ++;
        break;
      }
    }
  }

cout << ans << endl;

  return 0;
}