#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int dp[11][2][10];

int main() {

  string s; cin >> s;
  int N = s.size();

  dp[0][0][0] = 1;
  rep(i, N) rep(less, 2) rep(ones, 10) {
    auto const& curr = dp[i][less][ones];
    const int dmax = less ? 9 : s[i] - '0';
    rep(d, dmax + 1) {
      const bool nless = less || d < dmax;
      const int nones = ones + (d == 1);
      auto& next = dp[i+1][nless][nones];
      next += curr;
    }
  }

  int ans = 0;
  rep(i, 2) rep(j, 10) {
    ans += dp[N][i][j] * j;
  }

  cout << ans << endl;

  return 0;
}