#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int n;

void dfs(int i, int c, string s="") {
  if(i == n) { cout<<s<<endl; return; }
  dfs(i+1, 0, s+char('a'+c));
  if(c<2)dfs(i,c+1, s);
}

int main() {
  cin>>n;
  dfs(0, 0);

  return 0;
}