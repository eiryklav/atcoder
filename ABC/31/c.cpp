#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int main() {

  int n; cin >> n;
  int a[n];
  rep(i, n) {
    cin >> a[i];
  }

  int res = -inf;

  rep(i, n) {
    int smax = -inf, eidx = -1;
    rep(j, i) {
      int curr = 0;
      REP(k, j, i+1) {
        if((k - j) % 2) {
          curr += a[k];
        }
      }
      if(smax < curr) { smax = curr; eidx = j; }
    }

    REP(j, i+1, n) {
      int curr = 0;
      REP(k, i, j+1) {
        if((k - i) % 2) {
          curr += a[k];
        }
      }
      if(smax < curr) { smax = curr; eidx = j; }
    }

    int cand = 0;
    REP(k, min(i, eidx), max(i, eidx)+1) {
      if((k - min(i, eidx)) % 2 == 0) {
        cand += a[k];
      }
    }

//    cout << cand << " " << i << " " << eidx << endl;

    maximize(res, cand);
  }

  cout << res << endl;

  return 0;
}