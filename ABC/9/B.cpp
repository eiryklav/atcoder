#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int N; cin >> N;
  vector<int> A(N);
  rep(i, N) {
    cin >> A[i];
  }

  sort(A.begin(), A.end());
  A.erase(unique(A.begin(), A.end()), A.end());

  cout << A[A.size()-2] << endl;

  return 0;
}