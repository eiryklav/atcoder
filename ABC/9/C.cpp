#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int N, K;
string S;

int calc_inconsistency_with_S(std::string const& t, int front) {

}

int main() {

  cin >> N >> K;
  cin >> S;
  deque<char> stock;
  rep(i, N) stock.emplace_back(S[i]);
  sort(stock.begin(), stock.end());

  int idx = 0;
  int change = 0;
  string s = S;
  string ans;
  for(;idx < N && change < K;) {
    string u = s; int n = s.size();
    rep(i, n) {
      if(s[i] == stock[idx]) {
        u.erase(i, 1);
        break;
      }
    }

    auto r = calc_inconsistency_with_S(ans + stock[idx] + u, idx);

    idx++;

    if(change + r > K) { continue; }
    s = u;
    ans += stock[idx];
    change++;
  }

  return 0;
}