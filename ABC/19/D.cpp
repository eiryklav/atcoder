#include <iostream>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int dist[55][55];

int main() {

  int N; cin >> N;
  int max = 0, id = 0;
  REP(i, 2, N+1) {
    cout << '?' << ' ' << 1 << ' ' << i << endl;
    cin >> dist[1][i];
    if(max < dist[1][i]) {
      max = dist[1][i];
      id = i;
    }
  }

  int ans = 0;
  REP(i, 1, N+1) {
    if(id == i) { continue; }
    cout << '?' << ' ' << id << ' ' << i << endl;
    cin >> dist[id][i];
    ans = std::max(ans, dist[id][i]);
  }

  cout << '!' << ' ' << ans << endl;

  return 0;
}