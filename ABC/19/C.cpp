#include <iostream>
#include <algorithm>
#include <map>

using namespace std;

int main() {

  int N; cin >> N;
  int a[N];
  map<int, bool> mp;
  for(int i=0; i<N; i++) {
    cin >> a[i];
    mp[a[i]] = true;
  }

  sort(a, a+N);
  
  for(int i=N-1; i>=0; i--) {
    int curr = a[i];
    for(;curr > 0;) {
      if(curr % 2) { break; }
      curr >>= 1;
      mp[curr] = false;
    }
  }

  int ans = 0;
  for(auto e: mp) {
    ans += e.second == true;
  }

  cout << ans << endl;
  
  return 0;
}
