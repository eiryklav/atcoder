#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int const MOD = 1e9+7;

int main() {

  int N, M; cin >> N >> M;
  vector<int> f(N);
  rep(i, N) {
    cin >> f[i];
    f[i] --;
  }

  vector<ll> dp(N+1);
  dp[0] = 1;
  vector<int> num(M);
  int left = 0;
  ll sum = 1;
  rep(right, N) {
    num[f[right]] ++;
    while(num[f[right]] > 1) {
      num[f[left]] --;
      sum -= dp[left];
      sum = (sum % MOD + MOD) % MOD;
      left++;
    }
    dp[right+1] = sum;
    sum += dp[right+1];
    sum %= MOD;
  }

  cout << dp[N]%MOD << endl;

  return 0;
}