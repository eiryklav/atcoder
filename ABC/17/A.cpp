#include <iostream>

using namespace std;

int main() {

  int ans = 0;
  for(int s, e; cin >> s >> e; ) {
    ans+=s*e;
  }
  cout << ans << endl;
  return 0;
}
