#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int N, M; cin >> N >> M;
  vector<int> sum(M+2);
  int tot = 0;
  rep(i, N) {
    int l, r, s;
    cin >> l >> r >> s;
    sum[l] += s;
    sum[r+1] -= s;
    tot += s;
  }

  int min = sum[1];
  REP(i, 1, M+1) {
    sum[i+1] += sum[i];
    min = std::min(min, sum[i]);
  }

  cout << tot - min << endl;

  return 0;
}