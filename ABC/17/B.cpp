#include <iostream>

using namespace std;

int main() {
  string s; cin >> s;
  for(int i=1; i<s.size();i++) {
    if(s[i-1]=='c' && s[i] == 'h') {
      s.erase(s.begin()+i);
      s[i-1] = 'C';
      i--;
    }
  }
  bool ok = true;
  for(int i=0; i<s.size();i++) {
    ok = ok && (s[i] == 'C' || s[i] == 'o' || s[i] == 'k' || s[i] == 'u');
  }

  cout << (ok ? "YES\n":"NO\n");
  
  return 0;
}
