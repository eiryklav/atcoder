#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

typedef complex<double> P;

inline double cross(P const& a, P const& b) { return imag(conj(a)*b); }
inline double dot(P const& a, P const& b) { return real(conj(a)*b); }

bool check_ei(P a, P b, P c) {
  return
  acos(dot(b-a, c-a) / abs(b-a) / abs(c-a)) / acos(-1) < 0.5 &&
  acos(dot(c-b, a-b) / abs(c-b) / abs(a-b)) / acos(-1) < 0.5 &&
  acos(dot(a-c, b-c) / abs(a-c) / abs(b-c)) / acos(-1) < 0.5;
}

bool check_t(P a, P b, P c) {
  return
  acos(dot(b-a, c-a) / abs(b-a) / abs(c-a)) / acos(-1) == 0.5 ||
  acos(dot(c-b, a-b) / abs(c-b) / abs(a-b)) / acos(-1) == 0.5 ||
  acos(dot(a-c, b-c) / abs(a-c) / abs(b-c)) / acos(-1) == 0.5;
}

bool check_don(P a, P b, P c) {
  return
  acos(dot(b-a, c-a) / abs(b-a) / abs(c-a)) / acos(-1) > 0.5 ||
  acos(dot(c-b, a-b) / abs(c-b) / abs(a-b)) / acos(-1) > 0.5 ||
  acos(dot(a-c, b-c) / abs(a-c) / abs(b-c)) / acos(-1) > 0.5;
}

int main() {

//  cout << acos(dot(P(3,0), P(0, 4)) / abs(P(3, 0)) / abs(P(0, 4))) / acos(-1) << endl;

  int N ; cin >> N;

  assert(N <= 100);

  vector<P> v;

  rep(i, N) {
    int x, y; cin >> x >> y;
    v.push_back(P(x, y));
  }

  ll ei = 0, t = 0, don = 0;

  rep(i, N) REP(j, i+1, N) REP(k, j+1, N) {
    ei += check_ei (v[i], v[j], v[k]);
    t  += check_t  (v[i], v[j], v[k]);
    don+= check_don(v[i], v[j], v[k]);
  }

  cout << ei << " " << t << " " << don << endl;

  return 0;
}