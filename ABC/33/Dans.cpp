#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

double EPS = 1e-9;

int main() {

  int N; cin >> N;
  vector<int> x(N), y(N);
  rep(i, N) {
    cin >> x[i] >> y[i];
  }

  ll t = 0, don = 0;
  rep(i, N) {
    vector<double> a;
    rep(j, N) {
      if(i == j) { continue; }
      a.push_back(atan2(y[j] - y[i], x[j] - x[i]));
    }
    sort(all(a));
    rep(j, N-1) {
      a.push_back(a[j] + acos(-1) * 2);
    }
    rep(j, N-1) {
      int x = lower_bound(all(a), a[j] + acos(-1) / 2 - EPS) - a.begin();
      int y = upper_bound(all(a), a[j] + acos(-1) / 2 + EPS) - a.begin();
      int z = lower_bound(all(a), a[j] + acos(-1)) - a.begin();
      t += y - x;
      don += z - y;
    }
  }

  ll e = (ll) N * (N - 1) * (N - 2) / 6 - t - don;
  
  cout << e << " " << t << " " << don << endl;

  return 0;
}