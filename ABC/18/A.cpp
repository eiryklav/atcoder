#include <iostream>
#include <algorithm>
#include <map>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int a[3];
  rep(i,3) {
    cin>>a[i];
  }

  rep(i,3) {
    int k = 1;
    rep(j,3) {
      if(a[i]<a[j]) k++;
    }
    cout << k << endl;
  }
  
  return 0;
}