#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
ll const inf = 1e10;

template<class T> using PQ_G = priority_queue<T, vector<T>, greater<T>>;

void main_() {
  
  ll N, M, T; cin >> N >> M >> T;
  vector<ll> cs(N);
  rep(i, N) {
    cin >> cs[i];
  }

  vector<vector<pair<ll, int>>> G(N);
  vector<vector<pair<ll, int>>> revG(N); // 逆辺にして0から目的地まで向かう = 目的地から0まで向かう

  rep(i, M) {
    int a, b; ll c; cin >> a >> b >> c;
    a --, b --;
    G[a].emplace_back(b, c);
    revG[b].emplace_back(a, c);
  }

  auto get_dist = [&](vector<vector<pair<ll, int>>>& G) {
    vector<ll> dist(N, inf);
    dist[0] = 0;
    PQ_G<pair<ll, int>> pq;
    pq.emplace(0, 0);
    while(!pq.empty()) {
      ll c; int p; tie(c, p) = pq.top(); pq.pop();
      for(auto && e: G[p]) {
        int np; ll ec; tie(np, ec) = e;
        if(dist[np] <= c + ec) { continue; }
        dist[np] = c + ec;
        pq.emplace(dist[np], np);
      }
    }
    return std::move(dist);
  };

  auto go_dist = get_dist(G);
  auto back_dist = get_dist(revG);

  ll ans = 0;

  rep(i, N) {
    maximize(ans, (T - (go_dist[i] + back_dist[i])) * cs[i]);
  }

  cout << ans << endl;

}