#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int main() {

  string s, t; cin >> s >> t;
  set<char> st = {'a','t','c','o','d','e','r','@'};
  rep(i, s.size()) {
    if(s[i] == t[i]) {}
    else if(s[i] == '@' && st.count(t[i])) {}
    else if(t[i] == '@' && st.count(s[i])) {}
    else { cout << "You will lose\n";exit(0); }
  }
  cout << "You can win\n";

  return 0;
}