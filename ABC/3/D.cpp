#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

template<class value_type, int MOD>
class ModInt {
private:
  value_type val_;
  static value_type mod_pow(value_type x, value_type n, value_type mo) { value_type ret = 1; while(n > 0) { if(n & 1) { ret = ret * x % mo; } x = x * x % mo; n >>= 1; } return ret; }

public:

  ModInt() { val_ = 0; }
  ModInt(value_type x) { val_ = (x % MOD + MOD) % MOD; }

  ModInt const operator + (ModInt const& rhs) const {
    return std::move(ModInt(val_+rhs.get()));
  }

  ModInt const operator - (ModInt const& rhs) const {
    return std::move(ModInt(val_-rhs.get()));
  }

  ModInt const operator * (ModInt const& rhs) const {
    return std::move(ModInt(val_*rhs.get()));
  }

  ModInt const operator / (ModInt const& rhs) const {
    return std::move(ModInt(val_*mod_pow(rhs.get(), MOD-2, MOD)));  // fermat theorem
  }

  friend ModInt const operator + (value_type lhs, ModInt const& rhs) {
    return std::move(ModInt(lhs+rhs.get()));
  }

  friend ModInt const operator - (value_type lhs, ModInt const& rhs) {
    return std::move(ModInt(lhs-rhs.get()));
  }

  friend ModInt const operator * (value_type lhs, ModInt const& rhs) {
    return std::move(ModInt(lhs*rhs.get()));
  }

  friend ModInt const operator / (value_type lhs, ModInt const& rhs) {
    return std::move(ModInt(lhs*mod_pow(rhs.get(), MOD-2, MOD)));  // fermat theorem
  }
  
  ModInt operator += (ModInt const& rhs) {
    return *this = ModInt(val_+rhs.get()).get();
  }

  ModInt operator -= (ModInt const& rhs) {
    return *this = ModInt(val_-rhs.get()).get();
  }

  ModInt operator *= (ModInt const& rhs) {
    return *this = ModInt(val_*rhs.get()).get();
  }

  ModInt operator /= (ModInt const& rhs) {
    return *this = ModInt(val_*mod_pow(rhs.get(), MOD-2, MOD)).get();
  }

  bool operator == (ModInt const& rhs) const {
    return val_ == rhs.get();
  }

  value_type const get() const { return val_; }
  value_type &     get() { return val_; }

  friend ostream& operator << (ostream& ost, ModInt const& x) { 
    return ost << x.get();
  }

  friend istream& operator >> (istream& ist, ModInt& x) { 
    string s; ist >> s;
    int size = s.size();
    x.get() = 0;
    rep(i, size) {
      x.get() *= 10;
      x.get() += s[i]-'0';
      x.get() %= MOD;
    }
    return ist;
  }
};

typedef ModInt<long long, 1000000007> mint;

mint comb[1010][1010];
void make_comb(int n) {
  rep(i, n+1) {
    comb[i][0] = 1;
    REP(j, 1, i+1) {
      comb[i][j] = comb[i-1][j-1] + comb[i-1][j];
    }
  }
}

int main() {

  make_comb(1001);

  ll R, C; cin >> R >> C;
  ll X, Y; cin >> X >> Y;
  int D, L; cin >> D >> L;

  mint res;
  rep(S, 1<<4) {
    int w = X, h = Y;
    if(S >> 0 & 1) { w--; }
    if(S >> 1 & 1) { w--; }
    if(S >> 2 & 1) { h--; }
    if(S >> 3 & 1) { h--; }
    if(w <= 0 || h <= 0) { continue; }
    bool sign = 0;
    rep(i, 4) {
      if(S >> i & 1) { sign ^= 1; }
    }
    mint x = comb[w * h][D] * comb[w * h - D][L];
    if(sign) { res -= x; }
    else { res += x; }
  }

  res *= (C - X + 1) * (R - Y + 1);
  cout << res << endl;

  return 0;
}