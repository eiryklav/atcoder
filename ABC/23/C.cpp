#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <set>
#include <map>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int R, C, K, N;
  cin >> R >> C >> K >> N;
  int numR[R], numC[C];
  memset(numR, 0, sizeof numR);
  memset(numC, 0, sizeof numC);

  rep(i, N) {
    int r, c; cin >> r >> c;
    r--, c--;
    numR[r]++, numC[c]++;
  }

  sort(numC, numC+C);

  int ans = 0;
  rep(i, R) {
    int rem = K-numR[i];
    ans += upper_bound(numC, numC+C, rem) - lower_bound(numC, numC+C, rem);
    // -1 pattern
  }

  cout << ans << endl;

  return 0;
}