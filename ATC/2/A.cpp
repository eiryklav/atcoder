#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

typedef long long ll;
int const inf = 1<<29;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int main() {

  int R, C;
  int sy, sx;
  int gy, gx;
  cin >> R >> C >> sy >> sx >> gy >> gx; sy--, sx--, gy--, gx--;
  char c[R][C];
  rep(i, R) rep(j, C) {
    cin >> c[i][j];
  }

  queue<pair<int, int>> q;
  q.emplace(sy, sx);
  vector<vector<int>> dist(R, vector<int>(C, inf));
  dist[sy][sx] = 0;
  while(!q.empty()) {
    int y, x; tie(y, x) = q.front(); q.pop();
    if(gy == y && gx == x) {
      cout << dist[gy][gx] << endl;
      return 0;
    }
    rep(i, 4) {
      int ny = y+dy[i], nx = x+dx[i];
      if(!in_range(ny, nx, R, C)) { continue; }
      if(c[ny][nx] == '#') { continue; }
      if(dist[ny][nx] != inf) { continue; }
      dist[ny][nx] = dist[y][x] + 1;
      q.emplace(ny, nx);
    }
  }
  
  return 0;
}