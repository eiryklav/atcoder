#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
ll const inf = 1LL<<55;

vector<ll> w, wsum;

ll dp[111][111];

ll c(int i, int j) {
  ll& ret = dp[i][j];
  if(ret + 1) { return ret; }
  if(j < i) { return 0; }
  if(i == j) { return 0; }
  ret = inf;
  REP(k, i, j) {
    minimize(ret, c(i, k) + c(k+1, j));
  }
  REP(k, i, j+1) {
    ret += w[k];
  }
  return ret;
}

int main() {
  int N; cin >> N;
  minus(dp);
  w.resize(N);
  rep(i, N) cin >> w[i];
  cout << c(0, N-1) << endl;
  return 0;
}