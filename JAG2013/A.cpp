#include <bits/stdc++.h>

using namespace std;

int N, W, H;
int X[100010], Y[100010];

int main() {

  cin >> N >> W >> H;
  
  for(int i=0;i<N;i++) {
    int x, y, w;
    cin >> x >> y >> w;
    int xl = x-w, xr = x+w;
    if(xl < 0) xl = 0;
    if(xr > W) xr = W;
    X[xl]++, X[xr]--;
    
    int yl = y-w, yr = y+w;
    if(yl < 0) yl = 0;
    if(yr > H) yr = H;
    Y[yl]++, Y[yr]--;
  }

  bool ok1 = true;
  bool ok2 = true;
  int a = 0;
  for(int i=0; i<W; i++) {
    a += X[i];
    if(a <= 0) { ok1 = false; }
  }

  a = 0;
  for(int i=0; i<H; i++) {
    a += Y[i];
    if(a <= 0) { ok2 = false; }
  }

  cout << ((ok1 || ok2) ? "Yes\n" : "No\n");
  
  return 0;
}
