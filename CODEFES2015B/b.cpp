#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int N, M; cin >> N >> M;

  int A[N];
  map<int, int> mp;
  rep(i, N) {
    cin >> A[i];
    mp[A[i]]++;
  }

  for(auto& e: mp) {
    if(e.second > N/2) {
      cout << e.first << endl;
      exit(0);
    }
  }

  cout << "?\n";

  return 0;
}