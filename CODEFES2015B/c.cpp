#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int N, M; cin >> N >> M;

  int R[N], P[M];

  rep(i, N) {
    cin >> R[i];
  }

  sort(R, R+N);

  rep(i, M) {
    cin >> P[i];
  }

  sort(P, P+M);

  if(N < M) { cout << "NO\n"; return 0; }

  int n = 0, m = 0;
  rep(_, N+1) {
    if(m == M) { cout << "YES\n"; return 0; }
    if(n == N) { cout << "NO\n"; return 0; }

    if(R[n] < P[m]) {
      n++;
    }
    else {
      n++;
      m++;
    }
  }

  return 0;
}