#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
ll const inf = 1LL<<60;

int N;

int main() {

  cin >> N;

  set<pair<ll, int>> F;
  F.emplace(inf, -1);
  set<pair<ll, int>> st;

  rep(i, N) {
    int S, C; cin >> S >> C;
    auto iter = F.lower_bound({S, 0});
    if(iter->first == ) {
      F.emplace(S, 1);
    }
  }

  return 0;
}