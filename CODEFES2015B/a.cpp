#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  string S; cin >> S;

  map<char, int> m;
  rep(i, S.size()) {
    m[S[i]]++;
  }

  for(auto& mm: m) {
    if(mm.second == 1) {
      cout << mm.first;// << endl;
    }
  }

  cout << S << endl;

  return 0;
}