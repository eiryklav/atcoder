#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)
#define watch(a) cout << #a" = " << a << endl;

using ll  = long long;
ll const inf = 1LL<<50;

int N, M;
vector<pair<int, int>> niku;

namespace tree {

template<class value_type>
struct segtree {
  int N; 
  vector<value_type> dat, sum;
  segtree(int n) {
    N = 1;
    while(N < n) N *= 2;
    dat.assign(2 * N - 1, 0);
    sum.assign(2 * N - 1, 0);
  }
 
  void add(int a, int b, value_type x) { add(a, b, x, 0, 0, N); }
  void update(int a, value_type x) { add(a, a+1, x, 0, 0, N); }
 
  value_type add(int a, int b, value_type x, int k, int l, int r) {
    if(b <= l || r <= a) { return dat[k]; }
    if(a <= l && r <= b) {
      sum[k] += x;
      return dat[k] += x;
    }
    int m = (l + r) / 2;
    return dat[k] = max(add(a, b, x, 2 * k + 1, l, m), add(a, b, x, 2 * k + 2, m, r)) + sum[k];
  }

  value_type get(int a) { return getMax(a, a+1, 0, 0, N); }
  value_type getMax(int a, int b) { return getMax(a, b, 0, 0, N); }
  value_type getMax(int a, int b, int k, int l, int r) {
    if(b <= l || r <= a) { return -1<<29; }
    if(a <= l && r <= b) { return dat[k]; }
    int m = (l + r) / 2;
    return max(getMax(a, b, 2 * k + 1, l, m), getMax(a, b, 2 * k + 2, m, r)) + sum[k];
  }
 
};

} // namespace tree

int main() {

  cin >> N >> M;
  rep(i, N) {
    int s, l; cin >> s >> l;
    niku.emplace_back(s, l);
  }

  sort(all(niku));

  tree::segtree<ll> stree1(M+1);
  tree::segtree<ll> stree2(M+1);

  rep(i, M+1) {
    stree2.add(i, i+1, -2LL * i);
  }

  rep(i, N) {
    auto const next = niku[i].first + niku[i].second;

    auto const r1 = niku[i].first == 0 ? 0 : stree1.getMax(0, niku[i].first) + niku[i].second;
    auto const t1 = stree1.get(next);
    if(t1 < r1) { stree1.update(next, r1 - t1); }

    auto C = 2LL * niku[i].first + niku[i].second;

    #define range(i) niku[i].first, niku[i].first + niku[i].second

    auto const r2 = stree2.getMax(range(i)) + C;
    auto const t2 = stree2.get(next);
    if(t2 < r2) { stree2.update(next, r2 - t2 - 2 * next); }

    auto new_val = max({t1, r1, t2, r2});
    auto k1 = stree1.get(next);
    if(k1 < new_val) { stree1.update(next, new_val - k1); }
    auto k2 = stree2.get(next);
    if(k2 < new_val) { stree2.update(next, new_val - k2 - 2 * next); }
  }

  cout << stree1.getMax(0, M + 1) << endl;

  return 0;
}
