#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

struct SegTree {
  int N; 
  vector<int> dat, sum;
  SegTree(int n) {
    N = 1;
    while(N < n) N *= 2;
    dat.assign(2 * N - 1, 0);
    sum.assign(2 * N - 1, 0);
  }

  void add(int a, int b, int x) { add(a, b, x, 0, 0, N); }

  int add(int a, int b, int x, int k, int l, int r) {
    if(b <= l || r <= a) { return dat[k]; }
    if(a <= l && r <= b) {
      sum[k] += x;
      return dat[k] += x;
    }
    int m = (l + r) / 2;
    return dat[k] = max(add(a, b, x, 2 * k + 1, l, m), add(a, b, x, 2 * k + 2, m, r)) + sum[k];
  }

  int getMax(int a, int b) { return getMax(a, b, 0, 0, N); }
  int getMax(int a, int b, int k, int l, int r) {
    if(b <= l || r <= a) { return -1<<29; }
    if(a <= l && r <= b) { return dat[k]; }
    int m = (l + r) / 2;
    return max(getMax(a, b, 2 * k + 1, l, m), getMax(a, b, 2 * k + 2, m, r)) + sum[k];
  }

};

int main() {

  int N; cin >> N;
  SegTree stree(100010);
  vector<pair<int, int>> intervals;
  rep(i, N) {
    int s, t; cin >> s >> t;
    intervals.emplace_back(s, t);
    stree.add(s, t, 1);
  }
/*
  rep(i, 11) {
    cout << stree.getMax(i, i+1) << endl;
  }
  exit(0);
*/
  int ans = inf;
  rep(rm, N) {

/*
cout << "------before-----\n";
  rep(i, 11) {
    cout << stree.getMax(i, i+1) << endl;
  }

  cout << "add: [" << intervals[rm].first << ", " << intervals[rm].second << "]\n";
cout << "-----------------\n";
*/
    stree.add(intervals[rm].first, intervals[rm].second, -1);
    minimize(ans, stree.getMax(0, 1e5 + 10));
/*
cout << "------after------\n";
  rep(i, 11) {
    cout << stree.getMax(i, i+1) << endl;
  }
cout << "-----------------\n";
*/
    stree.add(intervals[rm].first, intervals[rm].second, +1);
  }

  cout << ans << endl;

  return 0;
}