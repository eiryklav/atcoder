#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int main() {

  int N; cin >> N;
  string s; cin >> s;

  set<int> st;

//  int pnum = 0;
  REP(i, 1, 2*N) {
    if(st.count(i-1)) { continue; }
    if(s[i-1] != s[i]) {
      st.emplace(i-1);
      st.emplace(i);
    }
  }
  cout << (2 * N - st.size()) / 2 << endl;
  return 0;
}