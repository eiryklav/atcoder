#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int getRawAns(int test, string const& s_) {
  auto s = s_;
  reverse(all(s));
  int n = s.size();
  rep(i, n) {
    if(s[i] == '-') {
      test *= -1;
    }
    else {
      test = !test;
    }
  }
  return test;
}

const vector<string> DATA = {
  "",
  "-",
  "!",
  "--",
  "-!",
  "!-",
  "!!",
  "---",
  "--!",
  "-!-",
  "!--",
  "!!-",
  "-!!",
  "!-!",
  "!!!"
};

vector<string> res;
set<pair<int, int>> rawst, st2;

int main() {

  string s; cin >> s;

  REP(i, -2, 3) {
    int rawAns = getRawAns(i, s);
    rawst.emplace(i, rawAns);
  }

  rep(i, DATA.size()) {
    st2.clear();
    REP(j, -2, 3) {
      int rawAns = getRawAns(j, DATA[i]);
      st2.emplace(j, rawAns);
    }
    if(rawst == st2) {
      res.push_back(DATA[i]);
    }
  }

  if(res.empty()) assert(false);
  sort(all(res), [&](string const& a, string const& b){ return a.size() < b.size(); });

  cout << res[0] << endl;

  return 0;
}