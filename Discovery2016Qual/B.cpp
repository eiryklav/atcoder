#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

namespace tree {  // not verified

struct BIT {
  vector<int> bit;
  int n;

  BIT(int n) {
    bit.resize(n + 1);
    this->n = n;
  }

  int sum(int i) {
    int s = 0;
    while(i > 0) {
      s += bit[i];
      i -= i & -i;
    }
    return s;
  }

  void add(int i, int x) {
    while(i <= n) {
      bit[i] += x;
      i += i & -i;
    }
  }
};

}

namespace sort_algorithm {  // not verified

long long inversion_count(int n, vector<int> const& a) {
  long long ret = 0;
  tree::BIT bit(n);
  for(int j=0; j<n; j++) {
    ret += j - bit.sum(a[j]);
    bit.add(a[j], 1);
  }
  return ret;
}

}

typedef long long ll;
int const inf = 1<<29;

int N;
vector<int> A;

int main() {

  cin >> N;
  rep(i, N) { int a; cin >> a; A.push_back(a); }
  rep(i, N) { A.push_back(A[i]); }

  int min = inf;
  int st = -1;
  rep(i, N) {
    if(min > A[i]) {
      min = A[i];
      st = i;
    }
  }
//  cout << "MM: " << st <<", " << min << endl;

//  rep(i, 2*N) {   cout << A[i] << " "; }cout << endl;

  int ans = 1;
  int max = min;
  REP(i, st+1, st + N) {
    if(max > A[i]) {
      ans ++;
      max = A[i];
    }
    maximize(max, A[i]);
  }

  cout << ans << endl;


  return 0;
}