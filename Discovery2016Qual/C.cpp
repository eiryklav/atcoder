#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

string S;
string ans = "zzzzzzzzzzzzzzzzzzz";
string memo[10000];

string dfs(int k) {
//  cout << k   << ", " <<S << endl;

  if(!memo[k].empty()) { return memo[k]; }


  if(k == 0) {
   minimize(ans, S);
   return memo[k] = S;
  }
  else {
    int n = S.size();
    rep(i, n) {
      string t = S.substr(0, i);
      t += 'a';
      t += S.substr(i);
      auto te = S;
      S = t;
      minimize(memo[k], dfs(k-1));
      S = te;
    }

    rep(i, n) {
      auto te = S;
      if(i == 0) {
        S = S.substr(1);
      }
      else {
        S = S.substr(0, i) + S.substr(i+1);
      }
      minimize(memo[k], dfs(k-1));
      S = te;
    }

    rep(i, n) {
      auto te = S[i];
      S[i] = 'a';
      minimize(memo[k], dfs(k-1));
      S[i] = te;
    }

  }
  return memo[k];
}

int main() {

  cin >> S;
  int K; cin >> K;

  assert(S.size() <= 10);

  dfs(K);

  cout << ans << endl;

  return 0;
}