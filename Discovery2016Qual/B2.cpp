#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int N;
vector<int> A;

int main() {

  cin >> N;
//  assert(N <= 100);
  vector<int> nextv; int next = 0;
  rep(i, N) { int a; cin >> a; A.push_back(a); nextv.push_back(a); }
  sort(all(nextv));
//  nextv.erase(unique(all(nextv)), nextv.end());

  bool vis[N]; zero(vis);
  int min = inf;
  int st = -1;
  rep(i, N) {
    if(min > A[i]) {
      min = A[i];
      st = i;
    }
  }

  int vcnt = 1;
  int walk = st + 1;
  vis[st] = 1;
  next = 1;

  REP(i, st+1, inf) {

    walk ++;
    if(nextv[next] == A[i%N] && !vis[i%N]) {
      next ++;
      vis[i%N] = 1;
      vcnt ++;
    }
    else if(!vis[i%N]) {
    }

    if(vcnt >= N) {
      cout << (walk - 1) / N + 1 - ((walk - 1) % N == 0) << endl;
      break;
    }

  }

  return 0;
}