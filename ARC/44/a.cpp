#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

void ng() { cout << "Not Prime\n"; exit(0); }

int main() {
  int c;
  int N; cin >> N;
  if(N == 1) ng();
  for(int i=2;(ll)i*i<=N;i++) {
    if(N % i == 0) goto next;
  }
  goto baka;
next:
  if((N % 2) == 0) ng();
  if(N % 5 == 0) ng();
  c = 0;
  rep(_,inf){
    if(N == 0) { break; }
    c += N % 10;
    N /= 10;
  }
  if(c % 3 == 0) ng();

baka:
  cout << "Prime\n";

  return 0;
}