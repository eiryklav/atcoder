#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  int N = 11111;

  // 3人で分け合う O(3^N)         
  rep(a, 1 << N) {
    int remain = (1 << N) - 1 - a;
    for(int b = remain; b >= 0; b--) {
      b &= remain;
      int c = (1 << N) - 1 - a - b;

    }
  }  

  // 4人で分け合う O(4^N) 過不足なし
  rep(a, 1<<N) {
    int remain_a = (1 << N) - 1 - a;
    for(int b = remain_a; b >= 0; b--) {
      b &= remain_a;
      int remain_b = (1 << N) - 1 - a - b;
      for(int c = remain_b; c >= 0; c--) {
        c &= remain_b;
        int d = (1 << N) - 1 - a - b - c;

      }
    }
  }

  return 0;
}