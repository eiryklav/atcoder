#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

#define s(x) ((x)*(x))
#define dist(i, j) s(x[i]-x[j]) + s(y[i]-y[j])

int main() {

  int N; cin >> N;
  int x[N], y[N];
  rep(i, N) {
    cin >> x[i] >> y[i];
  }

  double ans = 0;
  rep(i, N) REP(j, i+1, N) {
    if(ans < dist(i, j)) {
      ans = dist(i, j);
    }
  }

  printf("%.10f\n", sqrt(ans));

  return 0;
}