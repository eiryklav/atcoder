#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int N;
int x[1000], y[1000], c[1000];

double getMax(double X, double Y) {
  double l = 0.0;
  rep(i, N) { maximize(l, c[i] * max(abs(X-x[i]), abs(Y-y[i]))); }
  return l;
}

double solveY(double x) {
  double yl = -1e5-1, yr = 1e5+1;
  rep(_, 200) {
    double yml = (2. * yl + yr) / 3.0;
    double ymr = (yl + 2. * yr) / 3.0;
    auto ll = getMax(x, yml);
    auto lr = getMax(x, ymr);
    if(ll < lr) {
      yr = ymr;
    } else {
      yl = yml;
    }
  }
  return getMax(x, (yl + yr) / 2.0);
}

double solve() {
  double xl = -1e5-1, xr = 1e5+1;
  rep(_, 200) {
    double xml = (2. * xl + xr) / 3.0;
    double xmr = (xl + 2. * xr) / 3.0;
    auto yl = solveY(xml);
    auto yr = solveY(xmr);
    if(yl < yr) {
      xr = xmr;
    } else {
      xl = xml;
    }
  }
  return solveY((xl + xr) / 2.0);
}

int main() {
  cin >> N;
  rep(i, N) cin >> x[i] >> y[i] >> c[i];
  printf("%.15f\n", solve());
  return 0;
}