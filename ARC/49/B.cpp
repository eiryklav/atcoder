#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  int N; cin >> N;

  int x[N], y[N], c[N];
  rep(i, N) {
    cin >> x[i] >> y[i] >> c[i];
  }

  double lb = 0, lu = 1e10;
  rep(_, 200) {
    double lm = (lb + lu) / 2.0;
    double xl = -inf, yl = -inf;
    double xr = inf, yr = inf;
    rep(i, N) {
      maximize(xl, x[i] - lm / c[i]);
      maximize(yl, y[i] - lm / c[i]);
      minimize(xr, x[i] + lm / c[i]);
      minimize(yr, y[i] + lm / c[i]);
    }
    if(xl <= xr && yl <= yr) {
      lu = lm;
    } else {
      lb = lm;
    }
  }

  printf("%.15f\n", (lb + lu) / 2.0);

  return 0;
}