#include <bits/stdc++.h>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

enum {
	none, lside, rside
};

int h[int(3*1e5)+10];

int main() {

	int N; cin >> N;
	rep(i, N) { cin >> h[i]; }
	h[N] = 1e9; N++;
	int state = none;
	int max_len = 0;
	int len = 0;
	rep(i, N-1) {
		if(h[i] < h[i+1] && state == none) {
			state = lside;
		}
		else if(state == none) { continue; }
		else if(h[i] > h[i+1] && state == lside) {
			state = rside;
		}
		else if(h[i] < h[i+1] && state == rside) {
			state = lside;
			len = 0;
		}
		len++;
		max_len = max(max_len, len);
	}

	cout << max_len+1 << endl;

	return 0;
}