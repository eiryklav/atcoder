#include <bits/stdc++.h>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

#define DEF_CIN(type,var) type var; std::cin >> var;

int N, K;
string S;

int const MOD = 1e9+7;

int memo[310][310][310];
bool vis[310][310][310];

int solve(int curr, int zero, int one)
{
	if(zero > K || one > K) { return 0; }
	if(curr == N) { return 1; }

	int& ret = memo[curr][zero][one];
	if(vis[curr][zero][one]) { return ret; }
	vis[curr][zero][one] = 1;

	if(S[curr] == '0' || S[curr] == '?') {
		(ret += solve(curr+1, zero+1, max(0, one-1))) %= MOD;
	}
	if(S[curr] == '1' || S[curr] == '?') {
		(ret += solve(curr+1, max(0, zero-1), one+1)) %= MOD;
	}

	return ret;
}

int main() {

	cin >> N >> K >> S;

	cout << solve(0, 0, 0) << endl;

	return 0;
}
