#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
 
typedef long long ll;
 
enum {
	lside, rside
};
 
int h[int(3*1e5)+10];
 
int main() {
 
	int N; cin >> N;
	rep(i, N) { cin >> h[i]; }
	int state = lside;
	int max_len = 1;
	int len = 0;
	rep(i, N-1) {
		if(h[i] < h[i+1] && state == lside) {
			len++;
		}
		else if(h[i] > h[i+1] && state == lside) {
			state = rside;
			len++;
		}
		else if(h[i] < h[i+1] && state == rside) {
			state = lside;
			len = 1;
		}
		else {
			len++;
		}
		max_len = max(max_len, len);
	}
 	
 	if(max_len == 1) cout << 1 << endl;
	else cout << max_len+1 << endl;

	return 0;
}