#include <bits/stdc++.h>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

int main() {
	int N, K; cin >> N >> K;
	int t[N];
	bool ok = false;
	rep(i, N) {
		cin >> t[i];
		if(i-2>=0) {
			if(t[i-2]+t[i-1]+t[i]<K){
				cout << i+1 << endl;
				ok = true;
				break;
			}
		}
	}
	if(!ok) cout << -1 << endl;
	return 0;
}