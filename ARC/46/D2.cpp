#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

ll lcm(int a, int b) {
  return a * b / __gcd(a, b);
}

int const MOD = 1e9+7;

namespace math { namespace integer {

template<class value_type> value_type mod_mul(value_type x, value_type k, value_type m) { if(k == 0) { return 0; } if(k % 2 == 0) { return mod_mul((x+x) % m, k/2, m); } else { return (x + mod_mul(x, k-1, m)) % m; } }
template<class value_type> value_type mod_pow(value_type x, value_type n, value_type mod) { if(n == 0) { return 1; } if(n % 2 == 0) { return mod_pow(mod_mul(x, x, mod) % mod, n / 2, mod); } else { return mod_mul(x, mod_pow(x, n - 1, mod), mod); } }
template<class value_type> value_type mod_inverse(value_type x, value_type mod) { return mod_pow(x, mod-2, mod); }

}}

namespace math {

int mod_comb(int n, int k, int p) {
  if(n < 0 || k < 0 || n < k) { return 0; }
  int e1, e2, e3;
  int a1 = mod_fact(n, p, e1), a2 = mod_fact(k, p, e2), a3 = mod_fact(n-k, p, e3);
  if(e1 > e2 + e3) { return 0; }
  return a1 * integer::mod_inverse(integer::mod_mul(a2, a3, p), p) % p;
}
}

int main() {

  int H, W; cin >> H >> W;
  int G = __gcd(H, W);
  int ans = 0;
  REP(x, 1, G) {
    int y = G - x;
    if(lcm(W/__gcd(x, W), H/__gcd(y, H)) == (ll)H*W/G) {
      ans += math::mod_comb(G, x, MOD);
      ans %= MOD;
    }
  }

  cout << ans << endl;

  return 0;
}