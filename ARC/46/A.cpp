#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int keta(int x) {
  stringstream ss; ss << x;
  return ss.str().size();
}

int main() {

  int N; cin >> N;
  if(N < 10) {
    cout << N << endl;
  }
  else {
  //    11 22 33 44 55 ... 99 111 222 333 444 555 ...
    int d = 1;
    int k = 0;
    for(int i=0; i<N; i++) {
      k++;
      if(k % 10 == 0) {
        d ++;
        k = 1;
      }
    }

    rep(i, d) {
      cout << k;
    }
    cout << endl;
  }
  return 0;
}