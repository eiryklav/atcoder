#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int lcm(int a, int b) {
  return (ll)a * b / __gcd(a, b);
}

int const MOD = 1e9+7;

namespace math { namespace integer {

template<class value_type> value_type mod_mul(value_type x, value_type k, value_type m) { if(k == 0) { return 0; } if(k % 2 == 0) { return mod_mul((x+x) % m, k/2, m); } else { return (x + mod_mul(x, k-1, m)) % m; } }
template<class value_type> value_type mod_pow(value_type x, value_type n, value_type mod) { if(n == 0) { return 1; } if(n % 2 == 0) { return mod_pow(mod_mul(x, x, mod) % mod, n / 2, mod); } else { return mod_mul(x, mod_pow(x, n - 1, mod), mod); } }
template<class value_type> value_type mod_inverse(value_type x, value_type mod) { return mod_pow(x, mod-2, mod); }

}}

namespace math {

int fact[10000000];
void make_fact(int n) {
  fact[0] = fact[1] = 1;
  REP(i, 2, n+1) {
    fact[i] = (ll)fact[i-1] * i % MOD;
  }
}

int mod_comb(int n, int k, int p) {
  if(n < 0 || k < 0 || n < k) { return 0; }
  return (ll)fact[n] * integer::mod_inverse((ll)fact[k] * fact[n-k] % MOD, (ll)p) % MOD;
}
}

int main() {

  int H, W; cin >> H >> W;
  int G = __gcd(H, W);
  math::make_fact(G+1);
  int ans = 0;
  REP(x, 1, G) {
    int y = G - x;
    int a = W / __gcd(W, x);
    int b = H / __gcd(H, y);
    int L = lcm(a, b);
    if(L == (ll)H*W / G) {
      ans += math::mod_comb(G, x, MOD);
      if(ans >= MOD) ans -= MOD;
    }
  }

  cout << ans % MOD << endl;

  return 0;
}