#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <set>
#include <map>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  int N; cin >> N;
  int A[N];
  rep(i, N) {
    cin >> A[i];
  }

  sort(A, A+N, greater<int>());
  int sum = 0;
  rep(i, N) {
    if(i % 2 == 0) {
      sum += A[i];
    }
  }

  cout << sum << endl;

  return 0;
}