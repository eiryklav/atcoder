#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <set>
#include <map>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

char ma[101][101];
int memo[101][101];
int H, W;

bool inside(int x, int y) {
  if(!(0<=x&&x<W&&0<=y&&y<H)) { return false; }
  return ma[y][x] == '.';
}

bool rec(int x, int y) {
  if(memo[y][x]>=0) { return memo[y][x]; }
  int& ret = memo[y][x];
  ret = 0;
  if(inside(x+1, y)) ret = ret || !rec(x+1, y);
  if(inside(x, y+1)) ret = ret || !rec(x, y+1);
  if(inside(x+1,y+1))ret = ret || !rec(x+1,y+1);
  return ret;
}

int main() {

  memset(memo, -1, sizeof memo);
  cin >> H >> W;
  rep(i, H) rep(j, W) cin >> ma[i][j];

  cout << (rec(0,0) ? "First" : "Second") << endl;

  return 0;
}