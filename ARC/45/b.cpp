#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

int const inf = 1<<29;

struct SegTree {
  int N; 
  vector<int> dat, sum;
  SegTree(int n) {
    N = 1;
    while(N < n) N *= 2;
    dat.assign(2 * N - 1, 0);
    sum.assign(2 * N - 1, 0);
  }
 
  void add(int a, int b, int x) { add(a, b, x, 0, 0, N); }
 
  int add(int a, int b, int x, int k, int l, int r) {
    if(b <= l || r <= a) { return dat[k]; }
    if(a <= l && r <= b) {
      sum[k] += x;
      return dat[k] += x;
    }
    int m = (l + r) / 2;
    return dat[k] = min(add(a, b, x, 2 * k + 1, l, m), add(a, b, x, 2 * k + 2, m, r)) + sum[k];
  }
 
  int getMin(int a, int b) { return getMin(a, b, 0, 0, N); }
  int getMin(int a, int b, int k, int l, int r) {
    if(b <= l || r <= a) { return inf; }
    if(a <= l && r <= b) { return dat[k]; }
    int m = (l + r) / 2;
    return min(getMin(a, b, 2 * k + 1, l, m), getMin(a, b, 2 * k + 2, m, r)) + sum[k];
  }
 
};

int N, M;

int main() {

  cin >> N >> M;

  SegTree stree(N + 10);
  vector<pair<int, int>> v;
  rep(i, M) {
    int l, r; cin >> l >> r; r++;
    v.emplace_back(l, r);
    stree.add(l, r, +1);
  }

  vector<int> ans;
  rep(i, M) {
    int l = v[i].first, r = v[i].second;
    stree.add(l, r, -1);
    if(stree.getMin(l, r) > 0) {
      ans.push_back(i + 1);
    }
    stree.add(l, r, +1);
  }

  cout << ans.size() << endl;
  rep(i, ans.size()) {
    cout << ans[i] << endl;
  }

  return 0;
}