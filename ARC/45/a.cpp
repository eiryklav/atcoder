#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)

typedef long long ll;

int main() {

  string S;
  bool first = 1;
  while(cin >> S) {
    if(!first) cout << " ";
    first = 0;
    if(S[0] == 'L') {
      cout << "<";
    }
    else if(S[0] == 'R') {
      cout << ">";
    }
    else {
      cout << "A";
    }
  }
  cout << endl;

  return 0;
}