#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int N, X;
vector<pair<int, int>> g[100001];
map<ll, ll> mp;

void dfs(int curr, int prev = -1, ll totalCost = 0) {
//  cout << curr << " " << prev << " " << totalCost << endl;
  mp[totalCost] ++;
  for(auto& e: g[curr]) {
    int tar = e.first, cost = e.second;
    if(tar == prev) { continue; }
    dfs(tar, curr, cost ^ totalCost);
  }
}

int main() {

  cin >> N >> X;
  rep(i, N-1) {
    int x, y, c; cin >> x >> y >> c; x --, y--;
    g[x].emplace_back(y, c);
    g[y].emplace_back(x, c);
  }

  dfs(0);

  if(X) {
    ll ans = 0;
    for(auto& e: mp) {
      int tar = e.first ^ X;
      ans += mp[tar] * e.second;
    }
    ans /= 2;
    cout << ans << endl;
  }
  else {
    ll ans = 0;
    for(auto& e: mp) {
      ans += (e.second - 1) * e.second / 2;
    }
    cout << ans << endl;
  }

  return 0;
}