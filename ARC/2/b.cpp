#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int main() {

  int a, b; cin >> a >> b;
  queue<int> q;
  map<int, int> mp;
  q.push(a)  ;
  while(!q.empty()) {
    auto p = q.front(); q.pop();
    int c = mp[p];
    if(p == b) { cout << c << endl; exit(0); }

    #define DODO(i) \
    if(mp.find(p+i) == mp.end()) {  \
      mp[p+i] = c+1;  \
      q.push(p+i);  \
    } \
    if(p - i >= 0 && mp.find(p-i) == mp.end()) {  \
      mp[p-i] = c+1;  \
      q.push(p-i);  \
    }

    DODO(1) DODO(5) DODO(10)
  }

  return 0;
}