#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  ll R, B, x, y; cin >> R >> B >> x >> y;

  // maximize R + B - (y * a + x * b)

  double beta = -1.0 / x;
  double gamma = (double)R / x;

  double alpha = -y;
  double delta = B;

  // Y <= beta * X + gamma
  // Y <= alpha * X + delta

  vector<pair<double, double>> cands;

  // 1. cross y = 0
  {
    double X1 = -gamma / beta;
    double X2 = -delta / alpha;
    double X = min(X1, X2);
    cands.emplace_back(X, 0);
  }

  // 2. cross x = 0
  {
    double Y1 = gamma;
    double Y2 = delta;
    double Y = min(Y1, Y2);
    cands.emplace_back(0, Y);
  }

  // 3. cross point
  {
    double X = (gamma - delta) / (alpha - beta);
    double Y = alpha * X + delta;
    cands.emplace_back(X, Y);
  }

  auto expr = [&](double a, double b) {
    return R + B - (a * y + b * x);
  };

  ll max = 0;
  rep(i, cands.size()) {
    maximize(max, (ll)expr(cands[i].first, cands[i].second));
  }

  cout << max << endl;
  
  return 0;
}