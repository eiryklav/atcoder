#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  ll R, B, x, y; cin >> R >> B >> x >> y;
  
  ll ok = 0;  // compare(ok) is always true
  ll ng = 1e18+10;    // compare(ng) is always false

  auto compare = [&](ll K) {
    if(K > R) { return false; }
    if(K > B) { return false; }
    return K <= (R - K) / (x - 1) + (B - K) / (y - 1);
  };
  
  while(abs(ok-ng) > 1) { // ng極限の隣がokとなるまで
    ll mid = (ok + ng) / 2;
    (compare(mid) ? ok : ng) = mid;
  }

  cout << ok << endl;

  return 0;
}