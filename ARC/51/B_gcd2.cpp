#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;


#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int counter = 0;
int gcd(int a, int b) {
    if (b == 0) return a;
    counter++;
    return gcd(b, a%b);
}

int main() {
  cout << "       b =";
  REP(b, 1, 30)printf("%3d", b);
  cout << endl << "        ";
  REP(b, 1, 30*2) cout << "--";
  cout << endl;

  unordered_map<int, pair<int, int>> mp;
  REP(a, 1, 20000) {
//    printf("a = %3d | ", a);
    REP(b, 1, 20000) {
      counter = 0;
      gcd(a, b);
//      printf("%3d", counter);
      auto iter = mp.find(counter);
      mp[counter] = {a, b};
    }
//    cout << endl;
  }

  cout << "{";
  REP(i, 2, 41) {
    assert(mp.find(i) != mp.end());
    cout << "{" << mp[i].first << ", " << mp[i].second << "}" << ",";
  }
  cout << "}" << endl;
}










