#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

namespace math { namespace integer {

template<class value_type> value_type mod_mul(value_type x, value_type k, ll m) { if(k == 0) { return 0; } if(k % 2 == 0) { return mod_mul((x+x) % m, k/2, m); } else { return (x + mod_mul(x, k-1, m)) % m; } }
template<class value_type> value_type mod_pow(value_type x, value_type n, ll mod) { if(n == 0) { return 1; } if(n % 2 == 0) { return mod_pow(mod_mul(x, x, mod) % mod, n / 2, mod); } else { return mod_mul(x, mod_pow(x, n - 1, mod), mod); } }
template<class value_type> value_type extgcd(value_type a, value_type b, value_type& x, value_type& y) { value_type d = a; if(b != 0) { d = extgcd(b, a%b, y, x); y -= (a / b) * x;} else { x = 1, y = 0; } return d; }
template<class value_type> value_type mod_inverse(value_type x, ll mod) { return mod_pow(x, value_type(mod-2), mod); /* use fermat */ }
template<class value_type> value_type mod_inverse_composite_num_mod(value_type a, ll mod) { value_type x, y; extgcd(a, mod, x, y); return (mod + x % mod) % mod; }

}}
using namespace math::integer;

int main() {

  int N; ll A, B; cin >> N >> A >> B;
  vector<ll> a(N);
  rep(i, N) {
    cin >> a[i];
  }

  sort(all(a));
  ll bini = 0;
  while(1) {
    a[0] *= A;
    bini ++;
    if(bini < B) {}
    if(a[0] > a[1]) {
      break;
    }
  }

  sort(all(a));

  vector<int> ans(N);
  rep(i, N) {

  }
  
  return 0;
}