#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;


vector<pair<int, int>>ans = {{1, 1},{1, 2},{2, 3},{3, 5},{5, 8},{8, 13},{13, 21},{21, 34},{34, 55},{55, 89},{89, 144},{144, 233},{233, 377},{377, 610},{610, 987},{987, 1597},{1597, 2584},{2584, 4181},{4181, 6765},{6765, 10946},{10946, 17711}};

int main() {

  int k ;cin >> k;
  cout << ans[k-1].first << " " << ans[k-1].second << endl;
  
  return 0;
}