#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> using PQ_G = priority_queue<T, vector<T>, greater<T>>;

vector<int> tree[100001];

void main_() {

  int N; cin >> N;
  rep(i, N-1) {
    int a, b; cin >> a >> b;
    tree[a-1].push_back(b-1);
    tree[b-1].push_back(a-1);
  }

  PQ_G<int> pq; pq.push(0);
  vector<int> ans;
  set<int> vis;
  
  while(!pq.empty()) {
    auto p = pq.top(); pq.pop();
    vis.insert(p);
    ans.push_back(p);
    for(auto && e: tree[p]) {
      if(vis.count(e)) { continue; }
      pq.push(e);
    }
  }

  rep(i, N) {
    if(i) cout << " ";
    cout << ans[i] + 1;
  }

  cout << endl;

}