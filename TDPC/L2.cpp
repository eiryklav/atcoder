#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int N;
int f[1010][1010];
int sum[1010][1010];  // 猫iからみて、猫[0, j]が距離1以内にあるときの幸福度の総和
int dp[1010][1010];   // 猫iに着目して、猫[0, ...j]までの幸福度の最大値 に 猫[j, ...i]を距離1以内においたときの幸福度 を足したもの

int main() {

  cin >> N;
  rep(i, N) rep(j, N) {
    cin >> f[i][j];
  }

  rep(i, N) rep(j, i) {
    sum[i][j] = f[i][j] + (j > 0 ? sum[i][j-1] : 0);
  }

  REP(i, 1, N) {
    int prev = -inf;
    rep(j, i+1) {
      maximize(prev, dp[i-1][j]);
      dp[i][j] = prev + sum[i][i] - sum[i][j];
    }
  }

  int max = -inf;
  rep(i, N) maximize(max, dp[N-1][i]);
  cout << max * 2 << endl;  // 猫(i, j)について、幸福度は f[i][j] + f[j][i] = 2 * f[i][j]

  return 0;
}