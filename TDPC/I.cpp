#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

constexpr int INF = 1e9;

int N, dp[300][300], dp2[300][300];
string S;

auto rec2(int l, int r) {
  if(l > r) { return 0; }
  if(r - l < 2 || S[l] != 'i' || S[r] != 'i') { return -INF; }
  auto& ret = dp2[l][r];
  if(ret != -1) { return ret; }
  ret = -INF;
  REP(i, l+1, r) {
    if(S[i] == 'w') { ret = max(ret, rec2(l+1, i-1) + rec2(i+1, r-1) + 1); }
    if(S[i] == 'i') { ret = max(ret, rec2(l, i)     + rec2(i+1, r)); }
  }
  if(ret < 0) { ret = -INF; }

  return ret;
}

auto rec(int l, int r) {
  if(r - l < 2) { return 0; }
  auto& ret = dp[l][r];
  if(ret >= 0) { return ret; }

  ret = max(rec(l+1, r), rec(l, r-1));

  REP(i, l, r) ret = max(ret, rec(l, i) + rec(i+1, r));
  return ret = max(ret, rec2(l, r));
}


auto main() -> signed {
  cin >> S;
  N = S.size();
  memset(dp, -1, sizeof dp);
  memset(dp2, -1, sizeof dp2);
  cout << rec(0, N-1) << endl;
  return 0;
}