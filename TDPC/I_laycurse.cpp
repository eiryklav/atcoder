#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#define REP(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) REP(i,0,n)
 
int main(){
  int i,j,k,l,m,n;
  int a, b;
  static int dp[500][500];
  char in[1000];
  
  while(~scanf("%s",in)) {
  n = strlen(in);
 
  rep(l,n){
    rep(i,n-l){
      j = i + l;
      dp[i][j] = 0;
      REP(k,i,j){
        if(dp[i][j] < dp[i][k] + dp[k+1][j]) dp[i][j] = dp[i][k] + dp[k+1][j];
      }
      if(in[i]=='i' && in[j]=='i'){
        REP(k,i+1,j) if(in[k]=='w'){
          if(dp[i+1][k-1]*3 == k-i-1 && dp[k+1][j-1]*3 == j-k-1){
            if(dp[i][j] < dp[i+1][k-1] + dp[k+1][j-1] + 1) dp[i][j] = dp[i+1][k-1] + dp[k+1][j-1] + 1;
          }
        }
      }
    }
  }
 
  printf("%d\n",dp[0][n-1]);
 }
  return 0;
}
