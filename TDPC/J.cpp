#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int N;
int xs[16];

double memo[1<<16];

int getBit(int x, int i) {
  return x >> i & 1;
}

void setBit0(int& x, int i) {
  x &= ~(1<<i);
}

void setBit1(int& x, int i) {
  x |= 1<<i;
}

void drawBit(int x) {
  rep(i, 16) {
    cout << (x >> i & 1);
  }
  cout << endl;
}

double dfs(int stand) {

  if(stand == 0) { return 0.0; }
  if(memo[stand] >= 0) { return memo[stand]; }

  auto& ret = memo[stand];

  ret = 1e14;

  rep(i, 16) {

    if((i && (getBit(stand, i-1)) | getBit(stand, i) | getBit(stand, i+1)) == 0) { continue; }

    double expect = 1.0;
    double psum   = 0.0;

    if(i && getBit(stand, i-1)) {
      setBit0(stand, i-1);
      expect += dfs(stand) / 3.0;
      psum += 1.0 / 3.0;
      setBit1(stand, i-1);
    }
    if(getBit(stand, i)) {
      setBit0(stand, i);
      expect += dfs(stand) / 3.0;
      psum += 1.0 / 3.0;
      setBit1(stand, i);
    }
    if(getBit(stand, i+1)) {
      setBit0(stand, i+1);
      expect += dfs(stand) / 3.0;
      psum += 1.0 / 3.0;
      setBit1(stand, i+1);
    }

    // 自己ループのある遷移であることに注意する。メモ化再帰が正しく行われるように、漸化式を書いてE(bit)について解いてみると良い。
    // P(x)をxに投げる確率、E(bit)を残りの物の配列がbitであるときの投げる回数の期待値とする。
    // E(bit) = Σ {E(bit&~(1<<x)) * P(x)} + (1 - Σ P(x)) * E(bit) <=> E(bit) = Σ{E(bit&~(1<<x)) * P(x)} / Σ P(x)
    minimize(ret, expect / psum);
  }

  return ret;
}

int main() {

  minus(memo);

  cin >> N;  
  bitset<16> bs;
  rep(i, N) { cin >> xs[i]; }
  rep(i, N) { bs[xs[i]] = 1; }
  printf("%.10lf\n", dfs(bs.to_ulong()));

  return 0;
}