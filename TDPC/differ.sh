#!/bin/zsh
echo "- $3"
echo "compiling $1"
clang++ -std=c++1z $1
echo "running   $1"
./a.out<$3>__out1
echo "compiling $2"
clang++ -std=c++1z $2
echo "running   $2"
./a.out<$3>__out2
diff __out1 __out2
if [ $? -eq 0 ]; then
  echo "no diff"
  cat __out1
fi
rm __out1 __out2
