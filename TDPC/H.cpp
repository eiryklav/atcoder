#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int N, W, C;
int w[101], v[101], c[101];

int main() {

  cin >> N >> W >> C;
  rep(i, N) {
    cin >> w[i] >> v[i] >> c[i];
  }

  int dp[55][10001], temp[55][10001];
  zero(temp);

  rep(useCol, 51) {
    rep(i, 55) rep(j, 10001) dp[i][j] = temp[i][j];
    rep(i, N) REP(j, w[i], W+1) {
      if(useCol == c[i]) {
        rep(l, C+1) maximize(dp[l][j-w[i]], dp[l][j] + v[i]);
      }
    }
    REP(l, 1, C+1) rep(j, 10001) {
      maximize(temp[l-1][j], dp[l][j]);
    }
  }

  int max = 0;
  rep(i, 10001) maximize(max, dp[0][i]);
  cout << max << endl;

  return 0;
}