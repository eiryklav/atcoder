#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int const MOD = 1e9+7;

int dp[10001][2][101];

int main() {

  int D; string s; cin >> D >> s;
  int N = s.size();
  dp[0][1][0] = 1;
  rep(i, N) {
    rep(sum, D+1) {
      rep(d, 10) {
        (dp[i+1][0][(sum+d)%D] += dp[i][0][sum]) %= MOD;
      }
      rep(d, s[i]-'0') {
        (dp[i+1][0][(sum+d)%D] += dp[i][1][sum]) %= MOD;
      }
      (dp[i+1][1][(sum+s[i]-'0')%D] += dp[i][1][sum]) %= MOD;
    }
  }

  int res = (dp[N][0][0] + dp[N][1][0] - 1) % MOD;
  cout << res << endl;

  return 0;
}