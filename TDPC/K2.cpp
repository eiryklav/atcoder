#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

typedef pair<int, int> pii;

int N;
vector<pii> v;

int main() {

  cin >> N;

  rep(i, N) {
    int x, r; cin >> x >> r;
    v.emplace_back(x - r, x + r);
  }

  sort(v.begin(), v.end());
  rep(i, v.size()) v[i].second *= -1;

  vector<int> dp(N, inf);

  rep(i, N) {
    *lower_bound(all(dp), v[i].second) = v[i].second;
  }

  cout << lower_bound(all(dp), inf) - dp.begin() << endl;

  return 0;
}