#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <assert.h>
#include <unordered_map>

std::fstream fs("__atcoder.html");
std::string html_data;
std::vector<std::string> inputs;
std::vector<std::string> outputs;
int curr_pos = 0;

std::vector<std::pair<std::string, std::string>> predefined = {
  {"(空文字列)", ""}
};

void apply_predefined(std::string& str) {
  int n = str.size();
  for(int i=0; i<n; i++) {
    for(auto& e: predefined) {
      auto& bef = e.first;
      auto& aft = e.second;

      if(i+bef.size() > n) { continue; }
      bool ok = 1;
      for(int k=i; k<i+bef.size(); k++) {
        if(str[k] != bef[k-i]) { ok = 0; break; }
      }
      if(!ok) { continue; }

      str.replace(i, bef.size(), aft);
    }
  }
}

std::string parse_pre_tag() {
  auto open_tag  = html_data.find("<pre>", curr_pos);
  auto close_tag = html_data.find("</pre>", curr_pos);
  curr_pos  = close_tag + std::string("</pre>").size();
  if(open_tag == std::string::npos) { return ""; }

  auto ret = std::string(html_data.begin() + open_tag + std::string("<pre>\n").size(), html_data.begin() + close_tag);
  apply_predefined(ret);
  return ret;
}

int main(int argc, char** argv) {

  fs >> html_data;

  parse_pre_tag();

  while(1) {
    auto r = parse_pre_tag();
    if(r.empty()) { break; }
    inputs.push_back(r);

    auto s = parse_pre_tag();
    assert(!s.empty() && "出力例がありません");
    outputs.push_back(s);
  }
  
  
  
  return 0;
}