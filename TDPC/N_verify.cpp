#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int const MOD = 1e9+7;

struct UnionFind
{
  vector<int> rank;
  vector<int> rid;

  UnionFind(int n) {
    rank.resize(n);
    rid.assign(n, -1);
  }

  void unite(int u, int v) {
    u = root(u), v = root(v);
    if(u == v) { return ; }
    if(rank[u] < rank[v]) {
      rid[u] = v;
    }
    else {
      rid[v] = u;
      if(rank[u] == rank[v]) {
        rank[u]++;
      }
    }
  }

  bool same(int u, int v) {
    return root(u) == root(v);
  }

  int root(int x) {
    if(rid[x] < 0) return x;
    else return rid[x] = root(rid[x]);
  }
};

int N;
vector<pair<int, int>> g;

int main() {

  g.clear();

  cin >> N;
  rep(i, N-1) {
    int x, y;
    cin >> x >> y;
    x --, y --;
    if(x > y) { swap(x, y); }
    g.emplace_back(x, y);
  }

  sort(all(g));

  int ans = 0;

  do {
    UnionFind uf(N);
    uf.unite(g[0].first, g[0].second);
    bool ok = 1;
    REP(i, 1, N-1) {
      if(uf.same(g[0].first, g[i].first) || uf.same(g[0].second, g[i].first) ||
         uf.same(g[0].first, g[i].second) || uf.same(g[0].second, g[i].second)) {}
      else { ok = 0; break; }
      uf.unite(g[0].first, g[i].first);
      uf.unite(g[0].first, g[i].second);
    }
    if(ok) {
      ans ++;
    }
  } while(next_permutation(all(g)));

  cout << ans << endl;

  return 0;
}