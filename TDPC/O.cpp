#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

ll const MOD = 1e9+7;

namespace math { namespace combination {

#define COMBINATION_MAX 266

namespace memorized { namespace combination {
  enum { max_value = COMBINATION_MAX };
  bool __computed = false; inline bool computed() {if(__computed) { return true; } else { __computed = true; return false; }}
  ll dp[max_value][max_value];
}}

ll comb(int N, int K) {
  using memorized::combination::dp;
  if(memorized::combination::computed()) { return dp[N][K]; }

  // Make binomial coefficient
  using memorized::combination::max_value;

  rep(i, max_value) {
    dp[i][0] = 1;
    REP(j, 1, i+1) {
      dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
      dp[i][j] %= MOD;
    }
  }
  return dp[N][K];
}

}}

namespace math { namespace integer {

template<class value_type> value_type mod_mul(value_type x, value_type k, value_type m) { if(k == 0) { return 0; } if(k % 2 == 0) { return mod_mul((x+x) % m, k/2, m); } else { return (x + mod_mul(x, k-1, m)) % m; } }
template<class value_type> value_type mod_pow(value_type x, value_type n, value_type mod) { if(n == 0) { return 1; } if(n % 2 == 0) { return mod_pow(mod_mul(x, x, mod) % mod, n / 2, mod); } else { return mod_mul(x, mod_pow(x, n - 1, mod), mod); } }
template<class value_type> value_type mod_inverse(value_type x, value_type mod) { return mod_pow(x, mod-2, mod); }

}}

int main() {

  using math::combination::comb;
  using namespace math::integer;

  int N = 26;

  vector<int> freq;

  rep(i, N) { int x; cin >> x; if(x) { freq.push_back(x); } }

  if(freq.size() == 1) {
    cout << (freq[0] == 1 ? freq[0] : 0) << endl;
    exit(0);
  }

  N = freq.size();

  int sum[N]; sum[0] = freq[0];
  rep(i, N-1) sum[i+1] = sum[i] + freq[i+1];

  ll divs = 1;
  ll ans = 1;
  rep(i, N) {
    ans = mod_mul(ans, comb(sum[i], freq[i]), MOD);
    divs *= freq[i];
  }

  cout << mod_mul(ans, mod_inverse(divs, MOD), MOD) << endl;

  return 0;
}