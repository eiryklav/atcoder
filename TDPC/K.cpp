#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

ostream& operator << (ostream& ost, pair<int, int> const& p) {
  return ost << p.first << ", " << p.second;
}

template<class T>
ostream& operator << (ostream& ost, vector<T> const& c) {
  ost << "[ ";
  rep(i, c.size()) {
    if(i) ost << ", ";
    ost << c[i];
  }
  ost << " ]";
  return ost;
}

template<class value_type>
vector<value_type> longest_increasing_subsequence(vector<value_type> const& v) {

  int const N = v.size();

  constexpr value_type inf = numeric_limits<value_type>::max() / 4;

  vector<value_type> dp(N, inf);
  vector<int> indexes(N);
  rep(i, v.size()) {
    indexes[i] = lower_bound(dp.begin(), dp.begin() + N, v[i]) - dp.begin();
    dp[indexes[i]] = v[i];
  }

  vector<value_type> ret;
  rep(i, N) {
    if(dp[i] >= inf) { break; }
    ret.push_back(dp[i]);
  }
  return ret;
}

typedef pair<int, int> pii;

int N;
vector<pii> v;

int main() {

  cin >> N;

  rep(i, N) {
    int x, r; cin >> x >> r;
    v.emplace_back(x - r, x + r);
  }

  sort(v.begin(), v.end());

  vector<int> ans;
  rep(i, N) ans.push_back(v[i].second);
  cout << longest_increasing_subsequence(ans).size() << endl;

  return 0;
}