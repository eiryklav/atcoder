#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int N;
int f[1111][1111];
int sum[1111][1111];  // 猫iに関して(距離1制約を無視したとき)[0, j]までの幸福度の総和
int dp[1111][1111];

int main() {

  cin >> N;
  rep(i, N) rep(j, N) {
    cin >> f[i][j];
  }

  rep(i, N) rep(j, N) sum[i][j+1] = sum[i][j] + f[i][j];

  REP(i, 1, N) {
    int prev = -inf;
    rep(j, i+1) {
      maximize(prev, dp[i-1][j]);
      dp[i][j] = prev + sum[i][i] - sum[i][j];
    }
  }

  int max = -inf;
  rep(i, N) maximize(max, dp[N-1][i]);
  cout << max * 2 << endl;

  return 0;
}