#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int const MOD = 1e9+7;

namespace Matrix
{
  typedef vector<ll> Vec;
  typedef vector<Vec> Mat;

  Mat operator * (Mat& A, Mat& B) {
    Mat C(A.size(), Vec(B[0].size()));
    rep(i, A.size()) rep(k, B.size()) rep(j, B[0].size())
      C[i][j] = (C[i][j] + A[i][k] * B[k][j]) % MOD;
    return C;
  }

  void operator *= (Mat& A, Mat& B) {
    A = A*B;
  }

  Mat operator ^ (Mat& A, ll n) {
    Mat B(A.size(), Vec(A.size()));
    rep(i, A.size()) {
      B[i][i] = 1;
    }
    while(n > 0) {
      if(n & 1) { B *= A; }
      A *= A;
      n >>= 1;
    }
    return B;
  }
}

ll H; int R;
int g[16][16];
ll dp[1<<16][16];

int main() {

  cin >> H >> R;
  rep(i, R) rep(j, R) cin >> g[i][j];

  using namespace Matrix;
  Mat A(R, Vec(R));

  rep(st, R) {

    memset(dp, 0, sizeof dp);
    dp[1<<st][st] = 1;

    rep(S, 1<<R) {
      rep(i, R) {
        if(!dp[S][i]) { continue; }     // used=Sで最後にiを訪れた
        rep(j, R) {
          if(S >> j & 1) { continue; }  // used
          if(!g[i][j]) { continue; }    // not connected
          (dp[S|(1<<j)][j] += dp[S][i]) %= MOD;
        }
      }
    }

    rep(S, 1<<R) rep(i, R) (A[st][i] += dp[S][i]) %= MOD;
  }

  cout << (A^H)[0][0] << endl;

  return 0;
}