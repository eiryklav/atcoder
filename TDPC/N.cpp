#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

using ll  = long long;
int const inf = 1<<29;

int const MOD = 1e9+7;

ll comb[1111][1111];
void make_comb(int n) {
  rep(i, n+1) {
    comb[i][0] = 1;
    REP(j, 1, i+1) {
      comb[i][j] = comb[i-1][j-1] + comb[i-1][j];
      comb[i][j] %= MOD;
    }
  }
}


int N;

ll dummy;
ll dp[1111][1111];
ll treeSize[1111];
vector<int> g[1111];

ll rec(int curr, int prev) {

//  cerr << prev+1 << " " << curr+1 << endl;

  ll& ret = prev == -1 ? dummy : dp[curr][prev];
  if(ret >= 0) { return ret; }

  ret = 1;

  int csize = treeSize[curr] - 1;
  if(csize == 0) { return 1; }

  for(auto& next: g[curr]) {
    if(next == prev) { continue; }
    ret *= comb[csize][treeSize[next]];
    ret %= MOD;
    ret *= rec(next, curr);
    ret %= MOD;
    csize -= treeSize[next];
  }

//  assert(csize == 0);

  return ret % MOD;
}

int calcSize(int curr, int prev) {
  treeSize[curr] = 1;
  for(auto& next: g[curr]) {
    if(next == prev) { continue; }
    treeSize[curr] += calcSize(next, curr);
  }
  return treeSize[curr];
}

namespace math {
int extgcd(int a, int b, int& x, int& y) {
  int d = a;
  if(b != 0) {
    d = extgcd(b, a % b, y, x);
    y -= (a / b) * x;
  }
  else {
    x = 1; y = 0;
  }
  return d;
}

int mod_inverse(int a, int m) {
  int x, y;
  extgcd(a, m, x, y);
  return (m + x % m) % m;
}
}

int main() {

  make_comb(1110);

  rep(i, 1111) g[i].clear();

  vector<pair<int, int>> es;

  cin >> N;
  rep(i, N-1) {
    int x, y;
    cin >> x >> y;
    x --, y --;
    g[x].push_back(y);
    g[y].push_back(x);
    es.emplace_back(x, y);
  }

  // 解1) 解2みたいなことしなくても、mod_inverse使えばいいだろ
  minus(dp);
  int res = 0;
  rep(i, N) {
    calcSize(i, -1);
    dummy = -1; res += rec(i, -1); res %= MOD;
  }

  cout << (ll)res * math::mod_inverse(2, MOD) % MOD << endl;

  return 0;

  // 解2) 解を2で割ろうとしたとき、MODの除算のせいで解がずれるのを回避している。
  minus(dp);
  ll ans = 0, te = 0;
  vector<ll> lst;
  rep(i, N) {
    calcSize(i, -1);
    dummy = -1;
    ll r = rec(i, -1);
    te += r;
    if(te % 2 == 0) {
      te /= 2;
      lst.push_back(te);
      te = 0;
    }
    else {
      te %= MOD;
    }
  }

  rep(i, lst.size()) {
    ans += lst[i];
    ans %= MOD;
  }

  cout << ans << endl;
//  cout << ans / 2 << endl;
  return 0;
}
