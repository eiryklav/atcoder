#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int const MOD = 1e9+7;
int D, N;
string s;

int dp[10001][2][101];

int rec(int i, int lim, int sum) {

  int& ret = dp[i][lim][sum];
  if(ret >= 0) { return ret; }
  if(i == N) { return ret = sum == 0; }

  ret = 0;

  int dlimit = 9;
  if(lim) {
    dlimit = s[i]-'0';
  }

  rep(d, dlimit+1) {
    if(lim && d == dlimit) {
      (ret += rec(i+1, 1, (sum + d) % D)) %= MOD;
    }
    else {
      (ret += rec(i+1, 0, (sum + d) % D)) %= MOD;
    }
  }

  return ret;
}

int main() {

  cin >> D;
  cin >> s;
  N = s.size();

  if(D == 1) { cout << N << endl; return 0; }

  memset(dp, -1, sizeof dp);
  rec(0, 1, 0);

  cout << (dp[0][0][0] + dp[0][1][0]) % MOD << endl;

  return 0;
}