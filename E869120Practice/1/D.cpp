#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>
#include <stack>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int K, N;
string T;

void dfs() {

  stack<tuple<int, int, int, bool>> stk; // (idx, concat), branch(0, 1, 2), back
  stk.emplace(0, 0, -1, 0);
  while(!stk.empty()) {

    int idx, concat, branch; bool back; tie(idx, concat, branch, back) = stk.top();
    if(idx >= N) { stk.pop(); continue; }

    if(back) {

      if(branch == 0){}
      if(branch == 1) {
        if(K > 0) {
          T.erase(idx, 1);
          N --;
          K --;
        }
      }
      if(branch == 2){}

      stk.pop();

    }
    else {
      back = 1; get<3>(stk.top()) = 1;

      if(T[idx] < 's') {
        T = string(T.begin(), T.begin() + idx - min(K, concat)) + T.substr(idx);
        N -= min(K, concat);
        idx -= min(K, concat);
        K = max(0, K - concat);
        get<2>(stk.top()) = 0;
        stk.emplace(idx+1, 0, -1, 0);
      }
      else if(T[idx] == 's') {
        get<2>(stk.top()) = 1;
        stk.emplace(idx+1, concat+1, -1, 0);
      }
      else {
        get<2>(stk.top()) = 2;
        stk.emplace(idx+1, 0, -1, 0);
      }
    }
  }
}

void main_() {
//  for(;cin>>K>>T;) {
  cin >> K >> T;
//  K = 100000; srand(unsigned(time(NULL))); rep(i, K) T.push_back(rand() % 26 + 'a');
//  cout << K << ' ' << T << endl;
  N = T.size();
  dfs();
  cout << T << endl;
//}
}