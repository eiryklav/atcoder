#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

string LURD = "LURD";

void main_() {

  string S; int T; cin >> S >> T;
  int N = S.size();
  int y = 0, x = 0;
  int qcnt = 0;
  rep(i, N) {
    if(S[i] != '?') {
      int dir = LURD.find(S[i]);
      y += dy[dir], x += dx[dir];
    } else {
      qcnt ++;
    }
  }

  if(T == 1) {

    cout << abs(y) + abs(x) + qcnt << endl;

  } else {
    if(abs(y) + abs(x) > qcnt) {
      cout << abs(y) + abs(x) - qcnt << endl;
    } else {
      int diff = abs(abs(y) + abs(x) - qcnt);
      cout << diff % 2 << endl;
    }

  }

}