#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>
#include <stack>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int K, N;
string T;

void solve() {

  int pos = 0, concat = 0;
  while(1) {
    if(N == pos) { break; }
    if(N == 0) { return; }
    if(K == 0) { return; }
    if(T[pos] != 's') { pos ++; continue; }
    if(pos == N-1 || T[pos] > T[pos+1]) {
      K --;
      N --;
      T = string(T.begin(), T.begin() + pos - concat) + (pos + 1 < N ? T.substr(pos + 1) : "");
      concat = 0;
    }
    else if(T[pos] == T[pos+1]) {
      concat ++;
      pos ++;
    }
    else {
      concat = 0;
      pos ++;
    }
  }

  pos = N-1;
  while(1) {
    if(N == 0 || K == 0 || pos == -1) { return; }
    if(T[pos] == 's') {
      T.erase(pos, 1);
      N --;
      K --;
    }
    pos --;
  }

}

void main_() {
//  for(;cin>>K>>T;) {
  cin >> K >> T;
//  K = 100000; srand(unsigned(time(NULL))); rep(i, K) T.push_back(rand() % 26 + 'a');
//  cout << K << endl << T << endl;
  N = T.size();
  solve();
  cout << T << endl;
//}
}