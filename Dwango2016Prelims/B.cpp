#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  int N; cin >> N;
  int Ks[100001];
  priority_queue<pair<int, int>> pq;
  rep(i, N-1) {
    cin >> Ks[i];
    pq.emplace(-Ks[i], i);
  }

  vector<int> ans(N, -1);
  while(!pq.empty()) {
    auto p = pq.top(); pq.pop();
    if(ans[p.second] < 0) {
      ans[p.second] = -p.first;
    }
    if(ans[p.second + 1] < 0) {
      ans[p.second + 1] = -p.first;
    }
  }

  rep(i, ans.size()) {
    if(i) cout << " ";
    cout << ans[i];
  }

  cout << endl;

  return 0;
}