#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
//ll const Inf = 1LL<<59;
int const inf = 1 << 29;

vector<pair<int, int>> v;
int PMax;

int dp[2001][2001];

int dfs(int pos, int idx) {
  auto& ret = dp[pos][idx];
  if(ret + 1) { return ret; }
  if(idx == v.size()) { return 0; }
  ret = inf;
  if(idx > 0 && v[idx-1].first == v[idx].first) {
    return ret = dfs(pos, idx + 1) + abs(v[idx].second - pos);
  }
  REP(npos, pos, PMax + 1) {
    minimize(ret, dfs(npos, idx + 1) + abs(v[idx].second - npos));
  }
  return ret;
}

int main() {

  int N, L; cin >> N >> L;
  minus(dp);
  assert(N <= 2000 && L <= 2000);
  v.resize(N);
  rep(i, N) {
    cin >> v[i].first >> v[i].second;
    maximize(PMax, v[i].second);
  }

  sort(all(v));

  cout << dfs(0, 0) << endl;

  return 0;
}