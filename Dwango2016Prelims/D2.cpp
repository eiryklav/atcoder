#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
ll const Inf = 1LL<<59;

int N, M;
ll sum1[303][303];
ll sum2[303][303];
ll ms1[303][303];
ll ms2[303][303];
ll G[303][303];



int main() {

  cin >> N >> M;

  assert(N <= 50 && M <= 50);

  rep(i, N) rep(j, M) {
    cin >> G[i][j];
  }

  auto calcsum1 = [&](ll sum[][303]) {
    rep(i, N) rep(j, M) {
      sum[i][j] = G[i][j] + (j ? sum[i][j-1] : 0) + (i ? sum[i-1][j] : 0) - (i && j ? sum[i-1][j-1] : 0);
    }
  };

  auto calcsum2 = [&](ll sum[][303]) {
    for(int i=N-1; i>=0; i--) for(int j=M-1; j>=0; j--) {
      sum[i][j] = G[i][j] + (j + 1 < M ? sum[i][j+1] : 0) + (i + 1 < N ? sum[i+1][j] : 0) - (i + 1 < N && j + 1 < M ? sum[i+1][j+1] : 0);
    }
  };

  calcsum1(sum1);
  calcsum2(sum2);

  rep(i,)

  rep(i, N) {
    rep(j, M) {
      printf("%3lld ", sum2[i][j]);
    }
    puts("");
  }


  auto csum = [&](ll sum[][303], int i, int j, int k, int l) { // inclusive
    return sum[k][l] - (i ? sum[i-1][l] : 0) - (j ? sum[k][j-1] : 0) + (i && j ? sum[i-1][j-1] : 0);
  };

  // (i2, j2) に intersect しない maxrect

  ll ans = -Inf;
  rep(hui, N) rep(huj, M) {
    rep(msi, N) rep(msj, M) {
      if(hui <= msi && huj <= msj) { continue; }
      maximize(ans, sum1[msi][msj] + sum2[hui][huj]);
    }
  }

  cout << ans << endl;

  return 0;
}