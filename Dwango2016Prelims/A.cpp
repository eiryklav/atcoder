#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> vector<T> divisors(T x) {
  vector<T> ret;
  int p = 1;
  while((ll)p * p <= x) {
    if(x % p == 0) {
      ret.push_back(p);
      ret.push_back(x/p);
    }
    p ++;
  }
  return ret;
}

auto prime_factor = [](ll x){unordered_map<ll, int> xfact;for(int i=2; (ll)i*i<=x; i++) {while(x % i == 0) {xfact[i]++;x /= i;}}if(x > 1) xfact[x]++;return std::move(xfact);};

ll calc(unordered_map<ll, int>& umap) {
  ll ret = 1;
  for(auto && e: umap) {
    ret *= pow(e.first, e.second);
  }
}

int main() {

  ll N; cin >> N;

  string s;
  vector<map<ll, int>> v;
  rep(i, 4) {
    s += "25";
    auto l = stol(s);
//    auto v = divisors(l);
//    for(auto && e: v) { cout << e << " "; }cout << endl;
    v.push_back(prime_factor(l));
  }

  for(auto && mp25: v) {

  }

  return 0;
}