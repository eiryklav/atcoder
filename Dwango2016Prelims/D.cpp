#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
ll const Inf = 1LL<<59;

int N, M;
ll sum[303][303];
ll G[303][303];

ll mrect1[303][303];
ll mrect2[303][303];

int main() {

  rep(i, 303) rep(j, 303) mrect1[i][j] = mrect2[i][j] = -Inf;

  cin >> N >> M;

  assert(N <= 50 && M <= 50);

  rep(i, N) rep(j, M) {
    cin >> G[i][j];
  }

  rep(i, N) rep(j, M) {
    sum[i][j] = G[i][j] + (j ? sum[i][j-1] : 0) + (i ? sum[i-1][j] : 0) - (i && j ? sum[i-1][j-1] : 0);
  }

  auto csum = [&](int i, int j, int k, int l) { // inclusive
    return sum[k][l] - (i ? sum[i-1][l] : 0) - (j ? sum[k][j-1] : 0) + (i && j ? sum[i-1][j-1] : 0);
  };

  // (i, j) 左上で max な rectsize
  rep(i, N) rep(j, M) {
    REP(k, i, N) REP(l, j, M) {
      maximize(mrect2[i][j], csum(i, j, k, l));
    }
  }
  /*
  rep(i, N) {
    rep(j, M) {
      printf("%3lld ", mrect2[i][j]);
    }
    puts("");
  }
  */
  // (k, l) 右下で max な rectsize
  rep(k, N) rep(l, M) {
    rep(i, k+1) rep(j, l+1) {
      maximize(mrect1[k][l], csum(i, j, k, l));
    }
  }
  /*
  puts("");
  rep(i, N) {
    rep(j, M) {
      printf("%3lld ", mrect1[i][j]);
    }
    puts("");
  }
  */
  // (i2, j2) に intersect しない maxrect
  ll ans = -Inf;
  rep(hui, N) rep(huj, M) {
    rep(msi, N) rep(msj, M) {
      if(hui <= msi && huj <= msj) { continue; }
      maximize(ans, mrect1[msi][msj] + mrect2[hui][huj]);
    }
  }

  cout << ans << endl;

  return 0;
}