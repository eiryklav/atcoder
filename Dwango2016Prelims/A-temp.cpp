#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> vector<T> divisors(T x) {
  vector<T> ret;
  int p = 1;
  while((ll)p * p <= x) {
    if(x % p == 0) {
      ret.push_back(p);
      ret.push_back(x/p);
    }
    p ++;
  }
  return ret;
}

int main() {

  int N; cin >> N;
  auto d = divisors(N);
  
  ll ans = 0;
  for(auto && e: d) {
    stringstream ss; ss << e;
    string s; ss >> s;
    int n = s.size();
    bool ok = 1;
    rep(i, n) {
      if(i) { ok &= s[i] == '5'; }
      else { ok &= s[i] == '2'; }
    }
    if(ok) {
      ans ++;
    }
  }

  cout << ans << endl;

  return 0;
}