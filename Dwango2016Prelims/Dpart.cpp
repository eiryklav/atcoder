#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
ll const Inf = 1LL<<59;

// Sutras Phase (for practice)

ll dp[2525][2525];

int main() {

  int N, L; cin >> N >> L;
  assert(N <= 2000 || L <= 2000);

  vector<int> V = {0, L};

  int T[N], P[N];
  rep(i, N) {
    cin >> T[i] >> P[i];
    V.push_back(P[i]);
  }
  
  sort(all(V));
  
  V.erase(unique(all(V)), V.end());
  
  int M = V.size();
  
  rep(i, N) {
    P[i] = lower_bound(all(V), P[i]) - V.begin();
  }

  rep(i, 2525) rep(j, 2525) dp[i][j] = i == j ? 0 : Inf;

  rep(i, N) {
    if(i && T[i-1] == T[i]) {
      rep(j, M) {
        dp[i+1][j] = dp[i][j] + abs(V[j] - V[P[i]]);
      }
    }
    else {
      ll min = Inf;
      rep(j, M) {
        minimize(min, dp[i][j]);
        dp[i+1][j] = abs(V[j] - V[P[i]]) + min;
      }
    }
  }

  ll ans = Inf;
  rep(i, N) minimize(ans, dp[N][i]);
  cout << ans << endl;

  return 0;
}