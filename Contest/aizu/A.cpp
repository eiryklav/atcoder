#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

typedef complex<double> P;

const double EPS = 1e-7;

inline double cross(P const& a, P const& b) { return imag(conj(a)*b); }
inline double dot(P const& a, P const& b) { return real(conj(a)*b); }

struct L : public pair<P, P> {
  L(P const& a, P const& b) { first = a, second = b; }
  const P& operator[] (int x) const { return x == 0 ? first : second; }
  P& operator[] (int x) { return x == 0 ? first : second; }
};

P projection(const L &l, const P &p) {
  double t = dot(p-l[0], l[0]-l[1]) / norm(l[0]-l[1]);
  return l[0] + t*(l[0]-l[1]);
}

bool intersectSP(const L &s, const P &p) {
  return abs(s[0]-p)+abs(s[1]-p)-abs(s[1]-s[0]) < EPS; // triangle inequality
}


double distanceSP(const L &s, const P &p) {
  const P r = projection(s, p);
  if (intersectSP(s, r)) return abs(r - p);
  return min(abs(s[0] - p), abs(s[1] - p));
}

int main() {

  int x, y; cin >> x >> y;
  P t = P(x, y);
  int N; cin >> N;
  vector<P> T;
  rep(i, N) {
    int x, y; cin >> x >> y;
    T.emplace_back(x, y);
  }

  vector<L> LT;
  rep(i, N) {
    LT.emplace_back(T[i], T[(i+1)%T.size()]);
  }

  double ans = inf;

  rep(i, N) {
    minimize(ans, distanceSP(LT[i], t));
  }

  printf("%.15f\n", ans);
  
  return 0;
}