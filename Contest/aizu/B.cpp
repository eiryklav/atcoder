#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

struct union_find {
  vector<int> rank_, size_, rid_;
  union_find(int n) { rank_.resize(n); rid_.assign(n, -1); size_.resize(n, 1); }
  void operator()(int u, int v) {
    u = operator[](u), v = operator[](v);
    if(u == v) { return; }
    size_[u] = size_[v] = size_[u] + size_[v];
    if(rank_[u] < rank_[v]) { rid_[u] = v; }
    else { rid_[v] = u; if(rank_[u] == rank_[v]) { rank_[u]++; } }    
  }
  int operator[](int x) { if(rid_[x] < 0) return x; else return rid_[x] = operator[](rid_[x]); }
  int size_of(int x) { return size_[x]; }
};

int main() {

  int N, M; cin >> N >> M;

  vector<vector<int>> g(N);

  rep(i, M) {
    int u, v; cin >> u >> v; u --, v --;
    g[u].push_back(v);
    g[v].push_back(u);
  }

  vector<bool> vis(N);
  bool invalid = 0;

  std::function<void(int, int)> dfs = [&](int curr, int prev) {
    for(auto && e: g[curr]) {
      if(e == prev) { continue; }
      if(vis[e]) { invalid = 1; continue; }
      vis[e] = 1;
      dfs(e, curr);
    }
  };

  int ans = 0;

  rep(i, N) {
    if(!vis[i]) {
      vis[i] = 1;
      invalid = 0;
      dfs(i, -1);
      if(!invalid) { ans ++; }
    }
  }

  cout << ans << endl;
    
  return 0;
}