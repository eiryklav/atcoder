#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  static ll dp[2][5555];

  int N, P; cin >> N >> P;
  vector<pair<int, int>> vs;
  rep(i, N) {
    int a, b; cin >> a >> b;
    vs.emplace_back(a, b);
  }

  sort(all(vs), greater<pair<int, int>>());//[&](pair<int, int> const& a, pair<int, int> const& b) { return a.first > b.first; });

  rep(i, N) {
    rep(j, 5555) dp[(i+1)&1][j] = 0;
    rep(j, 5555) dp[(i+1)&1][j] = dp[i&1][j];
    rep(j, P+1) {
      maximize(dp[(i+1)&1][j + vs[i].first], dp[i&1][j] + vs[i].second);
    }
  }

  ll ans = 0;

  rep(j, 5555) {
    maximize(ans, dp[N&1][j]);
  }

  cout << ans << endl;
  
  return 0;
}