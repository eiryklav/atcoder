#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

struct unicycle_graph {
  
  /*
    assumed bidirectional graph
    1. check graph_type
    2. add all edges
    3. run build()
   */

  int N;

  using graph_type = pair<int, ll>; // to, cost;
  vector<vector<graph_type>> g;

  vector<int> cycle_vs_;
  vector<bool> in_cycle_v_;

  using edge = pair<int, ll>;  // end points
  set<edge> in_cycle_edge_;

  unicycle_graph(int n) : N(n), g(n), in_cycle_v_(n) {}

  void add_edge(int f, int t, int c) {
    g[f].emplace_back(t, c);
    g[t].emplace_back(f, c);
  }

  void build() {
    vector<bool> vis(N);
    vector<int> prev(N);

    std::function<bool(int, int)> dfs = [&](int x, int p) {
      for(auto const& e: g[x]) {
        int to, cost; tie(to, cost) = e;
        if(to == p) { continue; }
        prev[to] = x;
        if(vis[to]) {
          for(int curr = x; curr != to; curr = prev[curr]) {
            cycle_vs_.push_back(curr);
            in_cycle_v_[curr] = 1;
            in_cycle_edge_.emplace(prev[curr], curr);
            in_cycle_edge_.emplace(curr, prev[curr]);
          }
          cycle_vs_.push_back(to);
          in_cycle_v_[to] = 1;
          in_cycle_edge_.emplace(to, prev[to]);
          in_cycle_edge_.emplace(prev[to], to);
          reverse(all(cycle_vs_));
          return true;
        }
        else {
          vis[to] = 1;
          if(dfs(to, x)) { return true; }
        }
      }
      return false;
    };

    rep(i, N) {
      if(!vis[i]) { vis[i] = 1; if(dfs(i, -1)) { break; } }
    }
  }

  bool in_cycle(int x) { return in_cycle_v_[x]; }
  bool in_cycle_edge(int f, int t) { return in_cycle_edge_.count({f, t}); }
  vector<int> const& cycle_vertices() { return cycle_vs_; }

};

int count_components(vector<vector<pair<int, ll>>>& g) {

  int N = g.size();
  vector<bool> vis(N);
  std::function<void(int)> dfs = [&](int x) {
    for(auto && e: g[x]) {
      if(vis[e.first]) { continue; }
      vis[e.first] = 1;
      dfs(e.first);
    }
  };

  int ret = 0;
  rep(i, N) {
    if(!vis[i]) { vis[i] = 1; dfs(i); ret++; }
  }
  return ret;
}

int main() {

  ll N, M, K; cin >> N >> M >> K;
  unicycle_graph ug(N);
  vector<tuple<int, int, ll>> edges;
  rep(i, M) {
    int f, t; ll c; cin >> f >> t >> c; f --, t --;
    edges.emplace_back(f, t, c);
    ug.add_edge(f, t, c);
  }

  ug.build();

  K -= count_components(ug.g) - 1;
  if(K <= 0) { cout << 0 << endl; return 0; }

  vector<ll> ucosts, ccosts;
  rep(i, M) {
    int f, t; ll c; tie(f, t, c) = edges[i];
    if(ug.in_cycle_edge(f, t)){
      ccosts.push_back(c);
    } else {
      ucosts.push_back(c);
    }
  }


  ll sum1 = inf, sum2 = inf;
  // cycle を切らない
  if(ucosts.size() >= K-1) {
    sum1 = 0;
    sort(all(ucosts));
    rep(i, K-1) {
      sum1 += ucosts[i];
    }
  }

  // cycle を切る
  if(ccosts.size() > 1) {
    sort(all(ccosts));
    sum2 = ccosts[0] + ccosts[1];
    REP(i, 2, ccosts.size()) {
      ucosts.push_back(ccosts[i]);
    }

    sort(all(ucosts));

    rep(i, K-2) {
      sum2 += ucosts[i];
    }
  }

  cout << min(sum1, sum2) << endl;
  
  return 0;
}