#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

struct before_main{before_main(){cin.tie(0); ios::sync_with_stdio(false);}} before_main;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

namespace tree {

template<class value_type>
struct segtree {
  int N; 
  vector<value_type> dat, sum;
  segtree(int n) {
    N = 1;
    while(N < n) N *= 2;
    dat.assign(2 * N - 1, 0);
    sum.assign(2 * N - 1, 0);
  }
 
  void add(int a, int b, value_type x) { add(a, b, x, 0, 0, N); }
  void update(int a, value_type x) { add(a, a+1, x, 0, 0, N); }
 
  value_type add(int a, int b, value_type x, int k, int l, int r) {
    if(b <= l || r <= a) { return dat[k]; }
    if(a <= l && r <= b) {
      sum[k] += x;
      return dat[k] += x;
    }
    int m = (l + r) / 2;
    return dat[k] = max(add(a, b, x, 2 * k + 1, l, m), add(a, b, x, 2 * k + 2, m, r)) + sum[k];
  }

  value_type get(int a) { return getMax(a, a+1, 0, 0, N); }
  value_type getMax(int a, int b) { return getMax(a, b, 0, 0, N); }
  value_type getMax(int a, int b, int k, int l, int r) {
    if(b <= l || r <= a) { return -1<<29; } // MINIMUM
    if(a <= l && r <= b) { return dat[k]; }
    int m = (l + r) / 2;
    return max(getMax(a, b, 2 * k + 1, l, m), getMax(a, b, 2 * k + 2, m, r)) + sum[k];
  }
 
};

} // namespace tree

int main() {

  int N; cin >> N;
  tree::segtree<ll> st(N+2);
  vector<int> v = {0};
  rep(i, N) {
    int h; cin >> h;
    v.push_back(h);
    st.add(i+1, i+1, h);
  }
  v.push_back(0);
  N += 2;

  REP(i, 1, N-1) {
    int lmost = i;
    {
      int ok = i;
      int ng = 0;

      auto compare = [&](int lmost) {
        int val = st.getMax(lmost, i);
        return v[i] >= val;
      };
      
      while(abs(ok-ng) > 1) {
        int mid = (ok + ng) / 2;
        (compare(mid) ? ok : ng) = mid;
      }
      lmost = ok;
    }

    int rmost = i;
    {
      int ok = i;
      int ng = N-1;
      
      auto compare = [&](int rmost) {
        int val = st.getMax(i, rmost);
        return v[i] >= val;
      };
      
      while(abs(ok-ng) > 1) {
        int mid = (ok + ng) / 2;
        (compare(mid) ? ok : ng) = mid;
      }
      rmost = ok;
    }

    cout << rmost - lmost << endl;
  }
  
  return 0;
}