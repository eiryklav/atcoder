#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); int main() {
  ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;


int G1Len[1111];
int G2Len[1111];

vector<int> G1[1111];
vector<int> G2[1111];

int getMaxLen(vector<int> g[], int s, int n) {
  queue<int> q;
  q.push(s);
  vector<int> dist(n, -1);
  dist[s] = 0;
  int ret = 0;
  while(!q.empty()) {
    auto curr = q.front(); q.pop();
    for(auto && next: g[curr]) {
      if(dist[next] >= 0) { continue; }
      dist[next] = dist[curr] + 1;
      maximize(ret, dist[next]);
      q.push(next);
    }
  }
  return ret;
}

void main_() {
  
  int N1, M1; cin >> N1 >> M1;

  rep(i, M1) {
    int a, b; cin >> a >> b;
    G1[a].push_back(b);
    G1[b].push_back(a);
  }

  int N2, M2; cin >> N2 >> M2;

  rep(i, M2) {
    int a, b; cin >> a >> b;
    G2[a].push_back(b);
    G2[b].push_back(a);
  }

  rep(i, N1) {
    G1Len[i] = getMaxLen(G1, i, N1);
  }

  rep(i, N2) {
    G2Len[i] = getMaxLen(G2, i, N2);
  }

  int min = inf;
  int max = 0;

  rep(i, N1) rep(j, N2) {
    minimize(min, G1Len[i] + G2Len[j] + 1);
    maximize(max, G1Len[i] + G2Len[j] + 1);
  }

  rep(i, N1) {
    maximize(min, G1Len[i]);
  }
  rep(i, N2) {
    maximize(min, G2Len[i]);
  }

  cout << min << " " << max << endl;

}