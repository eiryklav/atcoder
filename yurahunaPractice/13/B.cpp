#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

constexpr int MOD = 1e9+7;

int main() {

  int N, K; cin >> N >> K;
  string S; cin >> S;
  const int OF = 333;

  int res = 0;

  rep(st, N) {
    int dp[333][2*OF+10] = {};
    dp[st][OF] = 1;
    REP(i, st, N) {
      REP(j, OF-K, OF+K+1) {
        if(S[i] == '1') {
          if(j+1 > OF+K) { continue; }
          dp[i+1][j+1] += dp[i][j];
          dp[i+1][j+1] %= MOD;
        }
        else if(S[i] == '0') {
          if(j-1 < OF-K) { continue; }
          dp[i+1][j-1] += dp[i][j];
          dp[i+1][j-1] %= MOD;
        }
        else {
          if(j+1 <= OF+K) dp[i+1][j+1] += dp[i][j], dp[i+1][j+1] %= MOD;
          if(j-1 >= OF-K) dp[i+1][j-1] += dp[i][j], dp[i+1][j-1] %= MOD;
        }
      }
    }

    REP(j, OF-K, OF+K+1) {
      if(st == 0) {
        res += dp[N][j];
        res %= MOD;
      }
      else {
        res -= dp[N][j];
      }
    }
  }

  cout << res << endl;

  return 0;
}