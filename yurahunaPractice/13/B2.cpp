#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int N, K;
string S;

constexpr int MOD = 1e9+7;

int dp[333][333][333];

int rec(int i, int zero, int one) {
  int& ret = dp[i][zero][one];
  if(ret + 1) { return ret; }

  if(zero > K) { return ret = 0; }
  if(one > K) { return ret = 0; }
  if(i == N) { return ret = 1; }

  ret = 0;

  if(S[i] == '0') {
    ret += rec(i+1, zero+1, max(0, one-1));
  }
  if(S[i] == '1') {
    ret += rec(i+1, max(0, zero-1), one+1);
  }
  if(S[i] == '?') {
    ret += rec(i+1, zero+1, max(0, one-1));
    ret += rec(i+1, max(0, zero-1), one+1);
  }

  ret %= MOD;

  return ret;
}

int main() {

  cin >> N >> K;
  cin >> S;

  minus(dp);
  cout << rec(0, 0, 0) << endl;
  return 0;
}