#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); int main() {
  ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int N, P;
vector<int> A, B;

int dp[5555][10555];

template<class T, class U>
void pair_rsort(vector<T>& a, vector<U>& b) {
  vector<pair<T, U>> v;
  rep(i, a.size()) v.emplace_back(a[i], b[i]);
  sort(v.rbegin(), v.rend());
  rep(i, a.size()) tie(a[i], b[i]) = v[i];
}

int solve() {

  pair_rsort(A, B);

  rep(i, N) rep(j, P+1) {
    maximize(dp[i+1][j], dp[i][j]);
    maximize(dp[i+1][j+A[i]], dp[i][j] + B[i]);
  }

  int ret = 0;
  rep(i, 5555) rep(j, 10555) {
    maximize(ret, dp[i][j]);
  }
  return ret;
}

void main_() {
  
  cin >> N >> P;
  A.resize(N), B.resize(N);
  rep(i, N) {
    cin >> A[i] >> B[i];
  }
  cout << solve() << endl;

}