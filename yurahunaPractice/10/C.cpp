#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

map<pair<string, bool>, bool> memo;

bool dfs(string& s, bool player) {

  if(memo.find({s, player}) != memo.end()) {
    return memo[{s, player}];
  }

  bool& res = memo[{s, player}];
  res = 0;

  bool pass = 1;

  char c[2] = {'o', 'x'};

  auto mine = c[player];
  auto yours = c[!player];

  if(s.find(yours) == string::npos) {
    return 1;
  }

  if(s[0] == yours) {
    auto i = s.find(mine, 1);
    if(i != string::npos) {
      pass = 0;
      string u = string(i+1, mine) + s.substr(i);
      res |= !dfs(u, !player);
    }
  }

  if(s.back() == yours) {
    auto i = s.rfind(mine, s.size() - 1);
    if(i != string::npos) {
      pass = 0;
      string u = s.substr(0, i) + string(s.size()-i+1, mine);
      res |= !dfs(u, !player);
    }
  }

  if(pass) {
    res |= !dfs(s, !player);
  }

  return res;
}

int main() {

  string s; cin >> s;
  cout << (dfs(s, 0) ? "o\n" : "x\n");

  return 0;
}