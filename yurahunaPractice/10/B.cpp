#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

struct xor128 {
  unsigned x,y,z,w;
  xor128(): x(89165727), y(157892372), z(7777777), w(757328) {}
  unsigned next() {
    unsigned t=x^(x<<11);
    x=y;y=z;z=w;
    return w=w^(w>>19)^t^(t>>8);
  }
  unsigned next(unsigned k) {
    return next()%k;
  }
} rndgen;


typedef long long ll;
int const inf = 1<<29;

vector<string> B = {
  "X.X.X",
  ".XXX.",
  "XXCXX",
  ".XXX.",
  "X.X.X"
};

vector<vector<bool>> F;

int dx[3] = {1,1,1};
int dy[3] = {-1,0,+1};

template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

int main() {

  int PH = B.size(), PW = B[0].size();

  rep(_, inf) {
    F.clear();
    F.resize(10);
    rep(i, 10) F[i].resize(10);

    set<pair<int, int>> st;
    rep(chal, 5) {
      int sy = rndgen.next(10);
      int sx = rndgen.next(10);
      st.emplace(sy, sx);
      rep(i, PH) rep(j, PW) {
        if(!in_range(i+sy, j+sx, 10, 10)) { continue; }
        if(B[i][j] != '.') {
          F[i+sy][j+sx] = 1;
        }
      }
    }

    queue<pair<int, int>> q;
    int res = 0;
    rep(i, 10) {
      if(!F[i][0]) { q.emplace(i, 0); }
    }

    while(!q.empty()) {
      int y, x; tie(y, x) = q.front(); q.pop();
      if(x == 9) {
        res ++;
        if(res > 1) { break; }
        continue;
      }
      rep(i, 3) {
        int ny = y + dy[i], nx = x + dx[i];
        if(!in_range(ny, nx, 10, 10)) { continue; }
        if(F[ny][nx]) { continue; }
        q.emplace(ny, nx);
      }
    }

    if(res == 1) {
      rep(i, 10) rep(j, 10) {
        if(st.count({i, j})) cout << 'C'; else cout << (F[i][j] ? 'X' : '.');
        if(j == 9) cout << endl;
      }
      cout << endl;
      rep(i, 10) rep(j, 10) {
        if(st.count({i, j})) cout << 'C'; else cout << '.';
        if(j == 9) cout << endl;
      }
      exit(0);
    }

  }

  return 0;
}