#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
 
using namespace std;
 
#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
 
typedef long long ll;
 
struct xor128 {
  unsigned x,y,z,w;
  xor128(): x(89165727), y(157892372), z(7777777), w(757328) {}
  unsigned next() {
    unsigned t=x^(x<<11);
    x=y;y=z;z=w;
    return w=w^(w>>19)^t^(t>>8);
  }
  unsigned next(unsigned k) {
    return next()%k;
  }
} rndgen;
 
string v[5] = {
  "x.x.x",
  ".xxx.",
  "xxcxx",
  ".xxx.",
  "x.x.x"
};
 
string V[5] = {
  "x.x.x",
  ".xxx.",
  "xxCxx",
  ".xxx.",
  "x.x.x"
};
 
 
constexpr bool in_range(int y, int x) {
  return 0<=x&&x<10 && 0<=y&&y<10;
}
 
void draw(vector<pair<int, int>> const& v) {
  vector<string> s = {
    "..........",
    "..........",
    "..........",
    "..........",
    "..........",
    "..........",
    "..........",
    "..........",
    "..........",
    ".........."
  }, t;
  t = s;
 
  rep(i, v.size()) {
    int x = v[i].first;
    int y = v[i].second;
    s[y][x] = 'C';
 
    //////
 
    t[y][x] = 'C';
    REP(a, -2, 3) REP(b, -2, 3) {
      if(!in_range(y+a, x+b)) { continue; }
      if(t[y+a][x+b] == 'C') { continue; }
      if(t[y+a][x+b] == 'x') { continue; }
      t[y+a][x+b] = V[a+2][b+2];
    }
  }
 
  rep(i, 10) {
    rep(j, 10) {
      cout << s[i][j];
    }
    cout << endl;
  }
 
  exit(0);
}
 
void solve() {
 
  rep(_, 10000) {
 
    bool G[10][10];
    memset(G, 0, sizeof G);
 
    vector<pair<int, int>> cpoint;
 
    rep(loop, 4) {
      int x = rndgen.next(10);
      int y = rndgen.next(10);
      cpoint.emplace_back(x, y);
      REP(k, -2, 3) REP(l, -2, 3) {
        if(!in_range(y+k, x+l)) { continue; }
        G[y+k][x+l] = v[k+2][l+2] == 'x' || v[k+2][l+2] == 'c';
      }
    }
/*
    rep(i, 10) {
      rep(j, 10) {
        cout << (G[i][j] ? 'x' : '.');
      }
      cout << endl;
    }
    exit(0);
*/
 
    queue<pair<int, int>> q;
    rep(i, 10) {
      if(!G[i][0]) { q.emplace(0, i); }
    }
 
    int ans = 0;
    while(!q.empty()) {
      auto p = q.front(); q.pop();
      int const x = p.first, y = p.second;
      if(x == 9) { ans++; if(ans > 1) goto next; }
      int nx = x+1, ny;
      REP(i, -1, 2) {
        ny = y + i;
        if(!in_range(nx, ny)) { continue; }
        if(G[ny][nx]) { continue; }
        q.emplace(nx, ny);
      }
    }
 
 
//    cout << "ok\n";
//    exit(0);
 
    if(ans == 1) {
      draw(cpoint);
      return;
    }
 
    next:;
  }
 
}
 
int main() {
 
  solve();
//  draw({{0,0}, {5,5}, {7,3}});
 
  return 0;
}