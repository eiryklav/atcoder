#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }
template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

typedef long long ll;
int const inf = 1<<29;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

int M, N;
int A[111][111], B[111][111];

namespace flow {  // not verified

struct edge {
  int to, cap, rev;
  edge(int t, int c, int r): to(t), cap(c), rev(r) {}
};

class dinic {

  static constexpr int MaxV = 5050;

  vector<edge> G[MaxV];

  int level[MaxV];
  int iter[MaxV];

  void bfs(int s) {
    minus(level);
    queue<int> q;
    level[s] = 0;
    q.push(s);
    while(!q.empty()) {
      auto v = q.front(); q.pop();
      for(auto && e: G[v]) {
        if(e.cap > 0 && level[e.to] < 0) {
          level[e.to] = level[v] + 1;
          q.push(e.to);
        }
      }
    }
  }

  int dfs(int v, int t, int f) {
    if(v == t) { return f; }
    for(auto && i = iter[v]; i < G[v].size(); i++) {
      auto && e = G[v][i];
      if(e.cap > 0 && level[v] < level[e.to]) {
        auto d = dfs(e.to, t, min(t, e.cap));
        if(d > 0) {
          e.cap -= d;
          G[e.to][e.rev].cap += d;
          return d;
        }
      }
    }
    return 0;
  }

public:

  void add_edge(int from, int to, int cap) {
    G[from].emplace_back(to, cap, G[to].size());
    G[to].emplace_back(from, 0, G[from].size()-1);
  }

  int max_flow(int s, int t) {
    int flow = 0;
    for(;;) {
      bfs(s);
      if(level[t] < 0) { return flow; }
      zero(iter);
      for(int f;(f = dfs(s, t, inf)) > 0; flow += f);
    }
  }

};
}

void main_() {

  cin >> N >> M;
  rep(i, N) rep(j, M) cin >> A[i][j];
  rep(i, N) rep(j, M) cin >> B[i][j];

  flow::dinic dn;

  int ans = 0;

  rep(i, N) rep(j, M) A[i][j] -= B[i][j];
  rep(i, N) rep(j, M) ans ++;
  rep(i, N) rep(j, M) if(A[i][j] == -1) dn.add_edge(5000, i*M+j, 1);
  rep(i, N) rep(j, M) if(A[i][j] ==  1) dn.add_edge(i*M+j, 5001, 1);

  rep(i, N) rep(j, M) {
    if(A[i][j] >= 0) { continue; }
    rep(k, 4) {
      int ni = i+dy[k], nj = j+dx[k];
      if(!in_range(ni, nj, N, M)) { continue; }
      if(A[ni][nj] != 1) { continue; }
      dn.add_edge(i*M+j, ni*M+nj, 1);
    }
  }

  ans -= dn.max_flow(5000, 5001);

  cout << ans << endl;
}