#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int main() {

  double X, Y; cin >> X >> Y;
  int x = X * 1000, y = Y * 1000;
  if(x == 0 && y == 0) {
    cout << "0 0 1 0\n0 0 0 1\n";
    return 0;
  }
  printf("%d %d %d %d\n", x, y, int((2 * X - x) * 1000), int((2 * Y - x) * 1000));
  printf("%d %d %d %d\n", x, -y, int((2 * X - x) * 1000), -int((2 * Y - x) * 1000));

  return 0;
}