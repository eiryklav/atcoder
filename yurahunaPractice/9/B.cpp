#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

struct union_find {
  vector<int> rank_;
  vector<int> size_;
  vector<int> rid_;

  union_find(int n) { rank_.resize(n); rid_.assign(n, -1); size_.resize(n, 1); }

  bool unite(int u, int v) {
    u = root(u), v = root(v);
    if(u == v) { return false; }
    size_[u] = size_[v] = size_[u] + size_[v];
    if(rank_[u] < rank_[v]) { rid_[u] = v; }
    else { rid_[v] = u; if(rank_[u] == rank_[v]) { rank_[u]++; } }
    return true;
  }

  bool same(int u, int v) { return root(u) == root(v); }
  int root(int x) { if(rid_[x] < 0) return x; else return rid_[x] = root(rid_[x]); }
  int operator[](int x) { return root(x); }
  int size_of(int x) { return size_[x]; }
  
};

ll lcm(ll x, ll y) {
  return x * y / __gcd(x, y);
}

int main() {

  int N, M, Q; cin >> N >> M >> Q;
  union_find uf(N+M);
  rep(i, Q) {
    int x, y; cin >> x >> y; x --, y --;
    uf.unite(x, y+N);
  }
  int L = lcm(N, M);
  int res = 0;
  rep(t, L) { if(uf.unite(t%N, t%M+N)) res = t + 1; }
  cout << res << endl;
  return 0;
}