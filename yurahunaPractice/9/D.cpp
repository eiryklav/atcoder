#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

typedef complex<double> P;

int main() {

  int N; cin >> N;
  vector<P> F(N), S(N);
  P fG, sG;
  rep(i, N) {
    double x, y; cin >> x >> y;
    F[i] = P(x, y);
    fG += F[i];
  }

  rep(i, N) {
    double x, y; cin >> x >> y;
    S[i] = P(x, y);
    sG += S[i];
  }

  fG /= N, sG /= N;

  double dF = 0.0, dS = 0.0;

  rep(i, N) {
    dF += abs(fG - F[i]);
    dS += abs(sG - S[i]);
  }

  printf("%.10f\n", dS / dF);

  return 0;
}