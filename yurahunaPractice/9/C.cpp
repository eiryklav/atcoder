#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

constexpr int MOD = 1e9+7;

namespace math { namespace integer {

template<class value_type> value_type mod_mul(value_type x, value_type k, ll m) { if(k == 0) { return 0; } if(k % 2 == 0) { return mod_mul((x+x) % m, k/2, m); } else { return (x + mod_mul(x, k-1, m)) % m; } }
template<class value_type> value_type mod_pow(value_type x, value_type n, ll mod) { if(n == 0) { return 1; } if(n % 2 == 0) { return mod_pow(mod_mul(x, x, mod) % mod, n / 2, mod); } else { return mod_mul(x, mod_pow(x, n - 1, mod), mod); } }
template<class value_type> value_type extgcd(value_type a, value_type b, value_type& x, value_type& y) { value_type d = a; if(b != 0) { d = extgcd(b, a%b, y, x); y -= (a / b) * x;} else { x = 1, y = 0; } return d; }
template<class value_type> value_type mod_inverse(value_type x, ll mod) { return mod_pow(x, mod-2, mod); /* use fermat */ }
template<class value_type> value_type mod_inverse_unusual_mod(value_type a, ll mod) { value_type x, y; extgcd(a, mod, x, y); return (mod + x % mod) % mod; }

}}
using namespace math::integer;

namespace math {

constexpr int MaxFact = 1000010;

struct FactRevFactList {

  array<long long, MaxFact+1> fact_;
  array<long long, MaxFact+1> rev_fact_;

  FactRevFactList() {
    fact_[0] = 1;
    for(int i=1; i<MaxFact; i++) {
      fact_[i] = fact_[i-1] * i;
      fact_[i] %= MOD;
    }

    rev_fact_[MaxFact-1] = mod_pow(fact_[MaxFact-1], (long long)MOD-2, MOD);

    for(int i=MaxFact-2; i>=0; i--) {
      rev_fact_[i] = rev_fact_[i+1] * (i+1);
      rev_fact_[i] %= MOD;
    }
  }

  long long fact(int x) const { return fact_[x]; }
  long long fact_inv(int x) const { return rev_fact_[x]; }

};

struct CombinationBig {
  math::FactRevFactList fr_;
  long long comb(int n, int r) { return (fr_.fact(n) * fr_.fact_inv(r) % MOD) * fr_.fact_inv(n-r) % MOD; }
};

}

math::CombinationBig cbig;

int main() {

  string s1, s2;
  int d1, d2;
  cin >> s1 >> d1 >> s2 >> d2;

  int n = s1.size();
  int same = 0, diff = 0;
  rep(i, n) {
    same += s1[i] == s2[i];
    diff += s1[i] != s2[i];
  }

  int ans = 0;
  rep(i, same + 1) {
    int dd1 = d1 - i;
    int dd2 = d2 - i;
    if(dd1 < 0 || dd2 < 0) { continue; }
    if(dd1 + dd2 != diff) { continue; }
    ll sc = mod_mul(cbig.comb(same, i), cbig.comb(diff, dd1), MOD);
    ans += sc;
    ans %= MOD;
  }

  cout << ans << endl;

  return 0;
}