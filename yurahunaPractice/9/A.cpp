#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
ll const inf = 1e10;

template<class T> bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

int dx[2] = {1,0};
int dy[2] = {0,1};

vector<vector<string>> F;
vector<vector<vector<ll>>> dist;

int main() {

  int N, H, W; cin >> N >> H >> W;
  F.resize(N);
  dist.resize(N);
  rep(i, N) {
    dist[i].resize(H);
    F[i].resize(H);
    rep(j, H) {
      dist[i][j].resize(W);
      rep(k, W) dist[i][j][k] = inf;
      cin >> F[i][j];
    }
  }

  priority_queue<tuple<ll, int, int, int>> pq;
  pq.emplace(0, 0, 0, 0);

  dist[0][0][0] = 0;

  while(!pq.empty()) {
    ll c; int f, y, x; tie(c, f, y, x) = pq.top(); pq.pop();
    c *= -1;  // コストは正

    if(f == N-1 && y == H-1 && x == W-1) { cout << c << endl; return 0; }

    rep(i, 2) {
      int ny = y + dy[i];
      int nx = x + dx[i];
      if(!in_range(ny, nx, H, W)) { continue; }
      int nf = f;
      if(F[f][ny][nx] == 'H') {
        nf ++;
      }
      int nc = c + F[nf][ny][nx] - '0';
      if(dist[nf][ny][nx] > nc) {
        dist[nf][ny][nx] = nc;
        pq.emplace(-dist[nf][ny][nx], nf, ny, nx);
      }
    }
  }

  return 0;
}