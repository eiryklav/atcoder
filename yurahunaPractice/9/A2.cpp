#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
ll const inf = 1e10;

template<class T> bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

int dx[2] = {1,0};
int dy[2] = {0,1};

vector<vector<string>> F;
vector<vector<vector<ll>>> dp;

int main() {

  int N, H, W; cin >> N >> H >> W;
  F.resize(N);
  dp.resize(N);
  rep(i, N) {
    dp[i].resize(H);
    F[i].resize(H);
    rep(j, H) {
      dp[i][j].resize(W, inf);
      cin >> F[i][j];
    }
  }

  dp[0][0][0] = 0;

  rep(k, N) rep(i, H) rep(j, W) {
    if(dp[k][i][j] == inf) { continue; }
    rep(d, 2) {
      int ni = i + dy[d], nj = j + dx[d];
      if(!in_range(ni, nj, H, W)) { continue; }
      int nk = k;
      if(F[nk][ni][nj] == 'H') { nk ++; }
      minimize(dp[nk][ni][nj], dp[k][i][j] + F[nk][ni][nj] - '0');
    }
  }

  cout << dp[N-1][H-1][W-1] << endl;

  return 0;
}