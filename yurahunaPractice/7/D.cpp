#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

template<class T> constexpr bool in_range(T y, T x, T H, T W) { return 0<=y&&y<H&&0<=x&&x<W; }

map<tuple<int, int, int, int>, int> dp;

int N, H, W;
int X[50], Y[50];

int dfs(int sy, int sx, int ty, int tx) {
  if(ty <= sy || tx <= sx) { return 0; }
  if(dp.find({sy, sx, ty, tx}) != dp.end()) { return dp[{sy, sx, ty, tx}]; }
  auto& ret = dp[{sy, sx, ty, tx}];

  ret = 0;
  rep(i, N) {
    if(!in_range(Y[i]-sy, X[i]-sx, ty-sy, tx-sx)) { continue; }
    int sum = (tx - sx) + (ty - sy) - 1;
    sum += dfs(sy, sx, Y[i], X[i]);
    sum += dfs(sy, X[i]+1, Y[i], tx);
    sum += dfs(Y[i]+1, sx, ty, X[i]);
    sum += dfs(Y[i]+1, X[i]+1, ty, tx);
    maximize(ret, sum);
  }

  return ret;
}

int main() {

  cin >> W >> H >> N;
  rep(i, N) {
    cin >> X[i] >> Y[i]; X[i]--, Y[i]--;
  }

  cout << dfs(0, 0, H, W) << endl;  

  return 0;
}
