#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int calc(string s, int x) {
  int n = s.size();
  reverse(all(s));
  rep(i, n) {
    if(s[i] == '-') { x *= -1; }
    if(s[i] == '!') { x = !x; }
  }
  return x;
}

vector<string> cand =
{
  "",
  "-",
  "!",
  "-!",
  "!-",
  "!!",
  "--!",
  "-!!",
  "!-!",
  "!--",
  "!!!"
};

int main() {

  string s; cin >> s;

//  for(int x;cin>>x;)cout << calc(s, x);

  rep(i, cand.size()) {
    bool ok = 1;
    REP(x, -256, 257) {
      ok &= calc(s, x) == calc(cand[i], x);
    }
    if(ok) {
      cout << cand[i] << endl;
      return 0;
    }
  }

  assert(false);

  return 0;
}