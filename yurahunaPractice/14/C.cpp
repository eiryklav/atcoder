#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

void main_(); signed main() {
  cin.tie(0); ios::sync_with_stdio(false);
  main_(); return 0;
}

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

void main_() {

  int N; cin >> N;
  int S[N], sum = 0;
  rep(i, N) cin >> S[i], sum += S[i];
  int M; cin >> M;
  vector<set<int>> knows(N);
  rep(i, M) {
    int a, b, c; cin >> a >> b >> c; b--, c--;
    if(a == 0) {
      knows[b].insert(c);
    }
    else {
      if(knows[b].count(c)) {
        cout << S[c] << " " << S[c] << endl;
      }
      else {
        int s = sum - S[b];
        for(auto && e: knows[b]) {
          s -= S[e];
        }

        int rem = N - knows[b].size() - 2;
        int min = std::max(0, s - rem * 100);
        int max = std::min(100, s);
        cout << min << " " << max << endl;
      }
    }
  }
}