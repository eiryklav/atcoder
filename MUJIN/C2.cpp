#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
template<class T1, class T2> inline bool minimize(T1 &a, T2 b) { return b < a && (a = b, 1); }
template<class T1, class T2> inline bool maximize(T1 &a, T2 b) { return a < b && (a = b, 1); }

typedef long long ll;
int const inf = 1<<29;

int N, M;
vector<int> g[20];
set<set<int>> paths;

set<int> my_path;
unordered_map<int, int> len_memo;

int dfs(int curr, int prev, int len) {

  int ret = 1;

  rep(i, N) {
    if(i == prev) { continue; }
    if(my_path.count(i)) {
      // odd & cycle が有れば return
      if((len - len_memo[i]) % 2) { continue; }
      else { return 1; }
    }
    else {
      my_path.insert(i);
      if(!paths.count(my_path)) {
        paths.insert(my_path);
        len_memo[i] = len + 1;
        ret *= dfs(i, curr, len + 1);
      }
      my_path.erase(i);
      len_memo.erase(i);
    }
  }

  return ret;
}

//int dfs_odd_cycles(int curr, int prev, set<int>& my_path)

int main() {

  cin >> N >> M;
  rep(i, M) {
    int x, y; cin >> x >> y; x--, y--;
    g[x].push_back(y);
    g[y].push_back(x);
  }

  int ans = 0;

  rep(i, N) {
    my_path = {i};
    ans += dfs(i, -1, 0);
  }

  cout << ans << endl;

  return 0;
}