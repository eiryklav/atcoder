#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int a[55], b[55];
int dp[55][10010][55];

int main() {

  int N, X, Y; cin >> N >> X >> Y;

  rep(i, N) cin >> a[i] >> b[i];

  rep(i, 55) rep(j, 10010) rep(k, 55) dp[i][j][k] = -inf;
  dp[0][0][0] = 0;

  rep(i, N) {
    rep(cnt, N+1) {
      int num = a[i]; // 個数とみなす
      for(int k = 1; num > 0; k <<= 1) {
        int mul = min(k, num);
        for(int ny = Y; ny >= b[i] - mul; ny--) {
          dp[i+1][ny][cnt+1] = std::max(dp[i+1][ny][cnt+1], dp[i][ny - b[i] + mul][cnt] + mul);
        }
        num -= mul;
      }
    }
  }

  rep(i, N+1) {
    if(dp[N][Y][i] >= X) {
      cout << i << endl;
      exit(0);
    }
  }

  cout << -1 << endl;

  return 0;
}