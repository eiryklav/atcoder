#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int N, X, Y;
int a[55], b[55];

int dp[55][10010][55];

int main() {

  cin >> N >> X >> Y;

  rep(i, N) cin >> a[i] >> b[i];
  if(accumulate(b, b+N, 0) < Y) {
    puts("-1");
    exit(0);
  }
  if(accumulate(a, a+N, 0) < X) {
    puts("-1");
    exit(0);
  }

  minus(dp);

  priority_queue<tuple<int, int, int, int>> pq;
  dp[0][0][0] = 0;
  pq.emplace(0, 0, 0, 0);
  int ans = inf;
  while(!pq.empty()) {
    int i, x, y, cnt; tie(i, x, y, cnt) = pq.top(); pq.pop();
    if(dp[i][y][cnt] > x) { continue; }
    if(x >= X && y >= Y) {
      minimize(ans, cnt);
      continue;
    }

    if(i == N) { continue; }
    
    rep(alpha, b[i]+1) {
      int nx = x + alpha;
      int ny = y - alpha;
      int nc = cnt + 1;

      if(y > ny) { continue; }
      if(dp[i+1][ny][nc] > nx) { continue; }
      dp[i+1][ny][nc] = nx;
      pq.emplace(i+1, nx, ny, nc);
    }
  }

  if(ans == inf) { cout << -1 << endl; }
  else { cout << ans << endl; }

  return 0;
}