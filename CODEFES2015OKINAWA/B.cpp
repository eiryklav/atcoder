#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int N, X, Y;
int a[55], b[55];
int dp[55][10010][55];

int findAlpha(int i, int y, int cnt) {
  int L = 0, R = b[i];
  while(1) {
    int ML = (2 * L + R) / 3;
    int MR = (L + 2 * R) / 3;
    int OL = L, OR = R;
    if(y + MR > Y) { R = MR; continue; }
    int lf = dp[i][y+ML][cnt];
    int rf = dp[i][y+MR][cnt];
    if(lf > rf) {
      R = MR;
    }
    else {
      L = ML;
    }
    if(OL == L && OR == R) { break; }
  }
  return L;
}

int main() {

  cin >> N >> X >> Y;

  rep(i, N) cin >> a[i] >> b[i];
  if(accumulate(b, b+N, 0) < Y) {
    puts("-1");
    exit(0);
  }
  if(accumulate(a, a+N, 0) < X) {
    puts("-1");
    exit(0);
  }

  rep(i, 55) rep(j, 10010) rep(k, 55) dp[i][j][k] = -inf;
  dp[0][0][0] = 0;

  rep(i, N) {
    rep(y, Y+1) {
      rep(cnt, N+1) {
        if(dp[i][y][cnt] < 0) { continue; }
        int alpha = findAlpha(i, y, cnt);
        int soup = b[i] - alpha;
        int noodle = a[i] + alpha;
        maximize(dp[i+1][y + soup][cnt+1], dp[i][y][cnt] + noodle);
      }
    }
  }

  rep(i, N+1) {
    if(dp[N][Y][i] >= X) {
      cout << i << endl;
      exit(0);
    }
  }

  cout << -1 << endl;

  return 0;
}