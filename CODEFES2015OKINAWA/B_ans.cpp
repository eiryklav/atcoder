#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int dp[55][11000];

int N, X, Y;

int main() {

  cin >> N >> X >> Y;
  minus(dp);
  dp[0][0] = 0;

  rep(i, N) {
    int a, b; cin >> a >> b;
    for(int j = i; j >= 0; j--) {
      rep(k, 10001) {
        if(dp[j][k] < 0) { continue; }
        maximize(dp[j + 1][min(10000, k + b)], dp[j][k] + a + b);
      }
    }
  }


  rep(i, N+1) {
    rep(j, 10001) {
      if(j < Y) { continue; }
      if(dp[i][j] >= X + Y) {
        cout << i << endl;
        return 0;
      }
    }
  }

  cout << -1 << endl;
  
  return 0;
}
