#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int N, X, Y;
int a[55], b[55];
int dp[55][10010][55];

int main() {

  cin >> N >> X >> Y;

  rep(i, N) cin >> a[i] >> b[i];
  if(accumulate(b, b+N, 0) < Y) {
    puts("-1");
    exit(0);
  }
  if(accumulate(a, a+N, 0) < X) {
    puts("-1");
    exit(0);
  }

  rep(i, 55) rep(j, 10010) rep(k, 55) dp[i][j][k] = -inf;
  dp[0][0][0] = 0;

  rep(i, N) {
    for(int cnt=N; cnt>=0; cnt--) {
      for(int y=10001; y>=b[i]; y--) {
        maximize(dp[i+1][y-b[i]][cnt], dp[i][y-b[i]][cnt]);
        maximize(dp[i+1][y][cnt+1], dp[i][y-b[i]][cnt] + a[i]); // 元のx, yの配分で一度DPする.
      }
    }
  }

  rep(i, N+1) {
    REP(y, Y, 10010) {
      if(dp[N][y][i] + y - Y >= X) {
        cout << i << endl;
        exit(0);
      }
    }
  }

  cout << -1 << endl;

  return 0;
}