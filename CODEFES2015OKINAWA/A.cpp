#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int main() {

  int H, W, K; cin >> H >> W >> K;

  int h = H / 2 + (H % 2 > 0);
  int w = W / 2 + (W % 2 > 0);

  int maxK = h * w;

  if(K > maxK) {  // ダメなら最大ケースで塗って確かめる
    cout << "IMPOSSIBLE\n"; exit(0);
  }

  char g[H][W]; rep(i, H) rep(j, W) g[i][j] = '.';
  int cnt = 0;
  rep(i, H) rep(j, W) {
    if(i % 2 == 1 || j % 2 == 1) {
    }
    else {
      g[i][j] = '#';
      cnt ++;
    }
    if(cnt == K) {

      rep(i, H) {
        rep(j, W) {
          cout << g[i][j];
        }
        cout << endl;
      }
      exit(0);
    }

  }

  return 0;
}