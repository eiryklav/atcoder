#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int N;
ll A;

ll calc_min(ll a, ll b) {
  return min(abs(a - b),  (ll)powl(10, N) - abs(a - b));
}

int main() {

  string s; cin >> s;
  A = stol(s);
  N = s.size();

  vector<int> v(10); iota(all(v), 0);
  ll max = 0, ansB = 0;
  do {
    ll B = 0;
    rep(i, N) {
      B *= 10;
      B += v[i];
    }
    ll r = calc_min(A, B);
    if(max < r) {
      max = r;
      ansB = B;
    }
  } while(next_permutation(all(v)));

  vector<int> ans(N);
  int pos = N-1;
  while(ansB > 0) {
    ans[pos--] = ansB % 10;
    ansB /= 10;
  }

  rep(i, N) {
    cout << ans[i];
  }
  cout << endl;

  return 0;
}