#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>
#include <cstring>
#include <random>
#include <functional>
#include <numeric>
#include <bitset>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)
#define all(c) (c).begin(), (c).end()
#define zero(a) memset(a, 0, sizeof a)
#define minus(a) memset(a, -1, sizeof a)
#define minimize(a, x) a = std::min(a, x)
#define maximize(a, x) a = std::max(a, x)

typedef long long ll;
int const inf = 1<<29;

int main() {

  string S, T; cin >> S >> T;
  vector<string> v = {S, T};
  int N = S.size();

  // S からスタート
  int scurr = 1;
  int pos[2] = {1, 0};
  REP(i, 1, N) {
    auto iter = find(v[i&1].begin() + pos[i&1], v[i&1].end(), S[scurr++]);
    if(iter != v[i&1].end()) {
      pos[i&1] = iter - v[i&1].begin() + 1;
    }
    else {
      goto next;
    }
  }

  cout << "Yes\n";
  exit(0);

next:;
  // T から
  scurr = 0;
  zero(pos);
  REP(i, 1, N+1) {
    auto iter = find(v[i&1].begin() + pos[i&1], v[i&1].end(), S[scurr++]);
    if(iter != v[i&1].end()) {
      pos[i&1] = iter - v[i&1].begin() + 1;
    }
    else {
      goto ng;
    }
  }

  cout << "Yes\n";
  exit(0);

ng:;
  cout << "No\n";

  return 0;
}